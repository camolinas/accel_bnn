# accel_bnn

Acelerador de rede neuronal binaria (BNN) implementado en FPGA.

![Alt text](./images/implementaciones.png)


### Folders
* gpu_cuda_bnn: Diseño del modelo BNN en esquema SMIT (Single Instruction Multiple Threads) con ecosistema CUDA en lenguaje C/C++.
* hardware_bnn: Proyecto en vivado v2019.2 de acelerador con modulo UART.
* litex_soc_bnn: Proyecto de acelerador conectado a procesador Vexrisc desarrollado con litex.
* software_model: Desarrollo de modelo CNN y BNN en Tensorflow, y simplificación de modelo con el paquete numpy. 

### Arquitectura Acelerador

![Alt text](./images/arquitectura_accel.png)


### Comando Litex
```c
lxterm /dev/ttyUSBX --kernel firmware/firmware.bin
```
