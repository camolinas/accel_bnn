TRIPLE=riscv32-unknown-elf
CPU=vexriscv
CPUFLAGS=-march=rv32im     -mabi=ilp32 -D__vexriscv__
CPUENDIANNESS=little
CLANG=0
CPU_DIRECTORY=/home/camilo/Documents/programs/litex_folder2/litex/litex/soc/cores/cpu/vexriscv
LITEX=1
COPY_TO_MAIN_RAM=1
EXECUTE_IN_PLACE=0
COMPILER_RT_DIRECTORY=/home/camilo/Documents/programs/litex_folder2/pythondata-software-compiler_rt/pythondata_software_compiler_rt/data
SOC_DIRECTORY=/home/camilo/Documents/programs/litex_folder2/litex/litex/soc
export BUILDINC_DIRECTORY
BUILDINC_DIRECTORY=/home/camilo/Documents/GeneralGit/accel_bnn/litex_soc_bnn/build/software/include
LIBCOMPILER_RT_DIRECTORY=/home/camilo/Documents/programs/litex_folder2/litex/litex/soc/software/libcompiler_rt
LIBBASE_DIRECTORY=/home/camilo/Documents/programs/litex_folder2/litex/litex/soc/software/libbase
LIBLITEDRAM_DIRECTORY=/home/camilo/Documents/programs/litex_folder2/litex/litex/soc/software/liblitedram
LIBLITEETH_DIRECTORY=/home/camilo/Documents/programs/litex_folder2/litex/litex/soc/software/libliteeth
LIBLITESPI_DIRECTORY=/home/camilo/Documents/programs/litex_folder2/litex/litex/soc/software/liblitespi
LIBLITESDCARD_DIRECTORY=/home/camilo/Documents/programs/litex_folder2/litex/litex/soc/software/liblitesdcard
BIOS_DIRECTORY=/home/camilo/Documents/programs/litex_folder2/litex/litex/soc/software/bios
