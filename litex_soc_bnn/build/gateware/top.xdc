################################################################################
# IO constraints
################################################################################
# serial:0.tx
set_property LOC V8 [get_ports serial_tx]
set_property IOSTANDARD LVCMOS33 [get_ports serial_tx]

# serial:0.rx
set_property LOC W8 [get_ports serial_rx]
set_property IOSTANDARD LVCMOS33 [get_ports serial_rx]

# clk125:0
set_property LOC K17 [get_ports clk125]
set_property IOSTANDARD LVCMOS33 [get_ports clk125]

# cpu_reset:0
set_property LOC Y16 [get_ports cpu_reset]
set_property IOSTANDARD LVCMOS33 [get_ports cpu_reset]

# user_led:0
set_property LOC M14 [get_ports user_led0]
set_property IOSTANDARD LVCMOS33 [get_ports user_led0]

# user_led:1
set_property LOC M15 [get_ports user_led1]
set_property IOSTANDARD LVCMOS33 [get_ports user_led1]

# user_led:2
set_property LOC G14 [get_ports user_led2]
set_property IOSTANDARD LVCMOS33 [get_ports user_led2]

# user_led:3
set_property LOC D18 [get_ports user_led3]
set_property IOSTANDARD LVCMOS33 [get_ports user_led3]

# user_display:0
set_property LOC N15 [get_ports user_display0]
set_property IOSTANDARD LVCMOS33 [get_ports user_display0]

# user_display:1
set_property LOC L14 [get_ports user_display1]
set_property IOSTANDARD LVCMOS33 [get_ports user_display1]

# user_display:2
set_property LOC K16 [get_ports user_display2]
set_property IOSTANDARD LVCMOS33 [get_ports user_display2]

# user_display:3
set_property LOC K14 [get_ports user_display3]
set_property IOSTANDARD LVCMOS33 [get_ports user_display3]

# user_display:4
set_property LOC N16 [get_ports user_display4]
set_property IOSTANDARD LVCMOS33 [get_ports user_display4]

# user_display:5
set_property LOC L15 [get_ports user_display5]
set_property IOSTANDARD LVCMOS33 [get_ports user_display5]

# user_display:6
set_property LOC J16 [get_ports user_display6]
set_property IOSTANDARD LVCMOS33 [get_ports user_display6]

# user_time:0
set_property LOC V12 [get_ports user_time0]
set_property IOSTANDARD LVCMOS33 [get_ports user_time0]

################################################################################
# Design constraints
################################################################################

################################################################################
# Clock constraints
################################################################################


create_clock -name clk125 -period 8.0 [get_nets clk125]

################################################################################
# False path constraints
################################################################################


set_false_path -quiet -through [get_nets -hierarchical -filter {mr_ff == TRUE}]

set_false_path -quiet -to [get_pins -filter {REF_PIN_NAME == PRE} -of_objects [get_cells -hierarchical -filter {ars_ff1 == TRUE || ars_ff2 == TRUE}]]

set_max_delay 2 -quiet -from [get_pins -filter {REF_PIN_NAME == C} -of_objects [get_cells -hierarchical -filter {ars_ff1 == TRUE}]] -to [get_pins -filter {REF_PIN_NAME == D} -of_objects [get_cells -hierarchical -filter {ars_ff2 == TRUE}]]