#!/usr/bin/env python3

from migen import *

from migen.genlib.io import CRG

from litex.build.generic_platform import *
from litex.build.xilinx import XilinxPlatform

from litex.soc.integration.soc      import SoCRegion
from litex.soc.integration.soc_core import *
from litex.soc.integration.builder import *
from litex.soc.cores import dna, xadc

from cores.ios import Led
from cores.v_ram.inst_ram import V_RAM
from cores.accel.inst_accel_bnn import ACCEL_BNN


# IOs ----------------------------------------------------------------------------------------------

_io = [

    ("clk125", 0, Pins("K17"), IOStandard("LVCMOS33")),

    ("cpu_reset", 0, Pins("Y16"), IOStandard("LVCMOS33")),

    ("serial", 0,
      Subsignal("tx", Pins("V8")),
      Subsignal("rx", Pins("W8")),
      IOStandard("LVCMOS33"),
      ),

    ("user_led",  0, Pins("M14"), IOStandard("LVCMOS33")),
    ("user_led",  1, Pins("M15"), IOStandard("LVCMOS33")),
    ("user_led",  2, Pins("G14"), IOStandard("LVCMOS33")),
    ("user_led",  3, Pins("D18"), IOStandard("LVCMOS33")),


    ("user_time",  0, Pins("V12"), IOStandard("LVCMOS33")),

    ("user_display",  0, Pins("N15"), IOStandard("LVCMOS33")),
    ("user_display",  1, Pins("L14"), IOStandard("LVCMOS33")),
    ("user_display",  2, Pins("K16"), IOStandard("LVCMOS33")),
    ("user_display",  3, Pins("K14"), IOStandard("LVCMOS33")),
    ("user_display",  4, Pins("N16"), IOStandard("LVCMOS33")),
    ("user_display",  5, Pins("L15"), IOStandard("LVCMOS33")),
    ("user_display",  6, Pins("J16"), IOStandard("LVCMOS33"))

];

# Platform -----------------------------------------------------------------------------------------

class Platform(XilinxPlatform):
    default_clk_name   = "clk125"
    default_clk_period = 8.0

    def __init__(self):
        XilinxPlatform.__init__(self, "xc7z020-clg400-1", _io, toolchain="vivado")

# Design -------------------------------------------------------------------------------------------

# Create our platform (fpga interface)
platform = Platform()

# Create our soc (fpga description)
class AccelSoC(SoCCore):
    def __init__(self, platform):
        sys_clk_freq = int(125e6)

        # SoC with CPU
        SoCCore.__init__(self, platform,
            cpu_type                 = "vexriscv",
            clk_freq                 = 125e6,
            ident                    = "LiteX CPU Test SoC", ident_version=True,
            integrated_rom_size      = 0x8000,
            integrated_main_ram_size = 0x4000)

        # Clock Reset Generation
        self.submodules.crg = CRG(platform.request("clk125"), platform.request("cpu_reset"))

        # UART parameters
        self.with_uart                = True;
        self.uart_name                = "serial";
        self.uart_baudrate            = 115200;
        self.uart_fifo_depth          = 16;

        # Timer parameters
        self.with_timer               = True;
        self.timer_uptime             = False;

        # FPGA identification
        self.submodules.dna = dna.DNA()
        self.add_csr("dna")

        # Led
        user_leds = Cat(*[platform.request("user_led", i) for i in range(4)])
        self.submodules.leds = Led(user_leds)
        self.add_csr("leds")

        # RAM Img 
        self.submodules.v_ram = V_RAM(platform)
        self.add_csr("v_ram")

        # accel bnn  
        user_displays = Cat( *[platform.request("user_display", i) for i in range(7)] )
        user_times = platform.request("user_time", 0)
        self.submodules.accel_bnn = ACCEL_BNN(platform, user_displays, user_times)
        self.add_csr("accel_bnn")        


soc = AccelSoC(platform)

# Build --------------------------------------------------------------------------------------------

builder = Builder(soc, output_dir="build", csr_csv="test/csr.csv")
builder.build(build_name="top")
