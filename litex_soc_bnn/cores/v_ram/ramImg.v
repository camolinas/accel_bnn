`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 03/08/2021 08:27:17 PM
// Design Name: 
// Module Name: ramImg_sim
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ramImg(
    input clk,
    input write_enable,
    input [9:0]addr,
    input [7:0]dataIn,
    
    output [7:0] dataOut
    );
    
    reg [7:0] mem [783:0];
    
    initial
     begin
        //$readmemb("/home/camilo/Documents/accelerator_BNN/code_test_acc/img28x28_n2_test.mem", mem);
        //$readmemb("/home/camilo/Documents/GeneralGit/accel_bnn/hardware_bnn/images_bin/img28x28_n3_bin.txt", mem);
        // $readmemb("/home/camilo/Documents/GeneralGit/accel_bnn/hardware_bnn/images_bin/img28x28_n9_bin.txt", mem);
        // $readmemb("/home/camilo/Documents/accelerator_BNN/code_test_acc/img28x28_n2_test.mem", mem);
        $readmemb("/home/camilo/Documents/GeneralGit/images_bin/img28x28_n8_bin.mem", mem);
     end
     
    always @(posedge clk)
        if(write_enable) mem[addr] <= dataIn;


    assign dataOut = mem[addr];       

endmodule     