#
# This file is not part of LiteX (8-P.
#
# Copyright (c) 2021 Dorfell Parra <dlparrap@unal.edu.cok>
# SPDX-License-Identifier: BSD-2-Clause
#
# Modified by: Camilo A. Molina

import os

from migen import *

from litex.soc.interconnect.csr import *
from litex.soc.integration.doc  import AutoDoc, ModuleDoc


# Helpers ------------------------------------------------------------------------------------------
def _to_signal(obj):
  return obj.raw_bits() if isinstance(obj, Record) else obj


# Simple PWM ------------------------------------------------------------------------------
class V_RAM(Module, AutoCSR, AutoDoc):
  """ RAM Memory - Save Image 
      Test code: Integrated verilog module and connected with cpu through CRS interface
  """


  def __init__(self, platform):
    self.intro   = ModuleDoc("RAM Mem. instance from verilog module");
    self.write_enable  = CSRStorage(1, reset=0x0, name="write_enable", description="""WE: Enable input to write into.""");
    self.addr  = CSRStorage(10, reset=0x000, name="addr", description="""addr: Input with mem address.""");
    self.dataIn  = CSRStorage(8, reset=0x00, name="dataIn", description="""dataIn: Input with the data to save.""");

    self.dataOut = CSRStatus(8, reset=0x00, name="dataOut", description="""dataOut: Output with the readed value.""");

    # Parameters
    self.params = dict(

      # CLK 
      i_clk = ClockSignal("sys"),

      # Decimal Number
      i_write_enable = self.write_enable.storage,
      i_addr = self.addr.storage,
      i_dataIn = self.dataIn.storage,

      # Display pines
      o_dataOut = self.dataOut.status
    );

    # Add VHDL sources.
    self.add_sources(platform);


  @staticmethod
  def add_sources(platform):
    sources = [

      # Core.
      "ramImg.v" ];

    # Direct use of VHDL sources.
    platform.add_sources("cores/v_ram/", *sources)


  def do_finalize(self):
    self.specials += Instance("ramImg", **self.params);
