`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06/15/2021 05:33:42 PM
// Design Name: 
// Module Name: accel_bnn
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module accel_bnn(
    input clk,
    input rst,
    
    input load,
    input [9:0]addr_loadImg,
    input [7:0]pixel,
    
    output ready_load,
    output reg finish_accel,
    output reg [3:0]class,
    
    output [7:0]check_pixel
      
    );
    
    reg [3:0]state;
    reg [3:0]next_state;
    
    // First Stage
    reg rst_stage;
    
    // RAM Result Batch
    reg wenable_batchMem;
    reg [7:0]addr_batchMem;
    reg [7:0]buff_addrbatch;
    reg [7:0]buff_addrbatch2; 
    reg [12:0]indata_batch;
    
    // Dense
    reg rst_dense;
    reg enable_dense;
    reg [38:0]input_dense;

    // First Stage
    wire finish_stage;
    wire [12:0]batch_out;
    
    // Batch Result
    wire [12:0]inDense0;
    wire [12:0]inDense1;
    wire [12:0]inDense2;
    
    // Dense
    wire finish_dense;
    wire finish_oneDense;
    wire [8:0]result0; 
    wire [8:0]result1; 
    wire [8:0]result2; 
    wire [8:0]result3; 
    wire [8:0]result4; 
    wire [8:0]result5; 
    wire [8:0]result6; 
    wire [8:0]result7;
    wire [8:0]result8; 
    wire [8:0]result9;
    
    // Higer value
    reg [8:0]buff0;
    reg [8:0]buff1;
    reg [8:0]buff2;
    reg [8:0]buff3;
    reg [8:0]buff4;
    reg [8:0]buff20;
    reg [8:0]buff21;
    reg [8:0]buff22;
    reg [3:0]buff_class0;
    reg [3:0]buff_class1;
    reg [3:0]buff_class2;
    reg [3:0]buff_class3;
    reg [3:0]buff_class4;
    reg [3:0]buff_class5;
        
    parameter S0 = 4'd0;
    parameter S1 = 4'd1;
    parameter S2 = 4'd2;
    parameter S3 = 4'd3;
    parameter S4 = 4'd4;
    parameter S5 = 4'd5;
    parameter S6 = 4'd6;
    parameter S7 = 4'd7;
    parameter S8 = 4'd8;
    parameter S9 = 4'd9;
    parameter S10 = 4'd10;
    parameter S11 = 4'd11;
    
    parameter S12 = 4'd12;
        
    
    first_stage stage(  .clk( clk ),
                        .rst( rst_stage ),
                        .load( load ),
                        .addr_loadImg( addr_loadImg ),
                        .pixel( pixel ), 
                        .ready_load( ready_load ),
                        .finish_inStage( finish_stage ),
                        .element( batch_out )

                    );                                        
                       
   
    ram_resultBatch resultB(    .clk( clk ),
                                .write_enable( wenable_batchMem ), 
                                .addr( addr_batchMem ),
                                .dataIn( indata_batch ),
                                .data1( inDense0 ),
                                .data2( inDense1 ),
                                .data3( inDense2 )
                            );                       
    
   
    dense_layer dense(  .clk( clk ),
                        .rst( rst_dense ),
                        .enable( enable_dense ),
                        .in_array( input_dense ),
                        .finish_dense( finish_dense ),
                        .finish_one( finish_oneDense ),
                        .result0( result0 ),
                        .result1( result1 ),
                        .result2( result2 ),
                        .result3( result3 ),
                        .result4( result4 ),
                        .result5( result5 ),
                        .result6( result6 ),
                        .result7( result7 ),
                        .result8( result8 ),
                        .result9( result9 )
                        
                     );                                             
    
    
    always @(posedge clk)
        if(rst) state <= S0;
        else    state <= next_state;
    

    always @(posedge clk)
        case(state)
            S0: begin
                if(load) next_state = S12;
                else next_state = S1;
                
                finish_accel = 1'b0;
                
                // First Stage
                rst_stage = 1'b1;

                // RAM Batch 
                wenable_batchMem = 1'b0;
                buff_addrbatch = 8'd0;
                
                // Dense
                rst_dense = 1'b1;
                enable_dense = 1'b0;
                end
            S1: begin
                rst_stage = 1'b0;
                if(finish_stage) begin
                                 next_state = S2;
                                 
                                 wenable_batchMem = 1'b1;
                                 addr_batchMem = buff_addrbatch;
                                 buff_addrbatch2 = buff_addrbatch;
                                 end
                else    begin
                        next_state = S1;
                        wenable_batchMem = 1'b0;
                        end
                end
            S2: begin
                if(addr_batchMem == 8'd155) next_state = S3;
                else    next_state = S1; 
                
                indata_batch = batch_out;
                buff_addrbatch = buff_addrbatch2 + 8'd1;
                end
            S3: begin                                                      // Prepare to Dense Layer
                next_state = S4;
                
                wenable_batchMem = 1'b0;
                
                rst_dense = 1'b0;
                enable_dense = 1'b1;
                
                buff_addrbatch = 8'd0;
                buff_addrbatch2 = 8'd0;
                addr_batchMem = 8'd0;
                end    
            S4: begin
                next_state = S5;
                
                enable_dense = 1'b1;
            
                input_dense = {inDense2, inDense1, inDense0};
                buff_addrbatch = buff_addrbatch2 + 8'd3;
                end
            S5: begin
                if(finish_oneDense) next_state = S6;
                else    next_state = S5;
                
                enable_dense = 1'b0;
                end    
            S6: begin
                if(finish_dense) next_state = S7;
                else next_state = S4;
                 
                addr_batchMem = buff_addrbatch;
                buff_addrbatch2 = buff_addrbatch;
                end
            S7: begin
                next_state = S8;
                if( result1 > result0 ) begin
                                        buff0 = result1;
                                        buff_class0 = 9'd1;
                                        end
                else begin
                     buff0 = result0;
                     buff_class0 = 9'd0;
                     end
                
                if( result3 > result2 ) begin
                                        buff1 = result3;
                                        buff_class1 = 9'd3;
                                        end
                else begin
                     buff1 = result2;
                     buff_class1 = 9'd2;
                     end
                
                end
            S8: begin
                next_state = S9;
                if( result5 > result4 ) begin
                                        buff2 = result5;
                                        buff_class2 = 9'd5;
                                        end
                else begin
                     buff2 = result4;
                     buff_class2 = 9'd4;
                     end
                
                if( result7 > result6 ) begin
                                        buff3 = result7;
                                        buff_class3 = 9'd7;
                                        end
                else begin
                     buff3 = result6;
                     buff_class3 = 9'd6;
                     end
                
                
                end
            S9: begin
                next_state = S10;
                
                if( result9 > result8 ) begin
                                        buff4 = result9;
                                        buff_class4 = 9'd9;
                                        end
                else begin
                     buff4 = result8;
                     buff_class4 = 9'd8;
                     end
            
                if( buff1 > buff0 ) begin
                                    buff20 = buff1;
                                    buff_class0 = buff_class1; 
                                    end
                else buff20 = buff0;        // class = buff_class0
                
                
                end
            S10: begin
                next_state = S11;
                if( buff3 > buff2 ) begin
                                    buff21 = buff3;
                                    buff_class2 = buff_class3;
                                    end
                else buff21 = buff2; // class = buff_class2
                
                if( buff4 > buff20 ) buff22 = buff4;
                else begin
                     buff22 = buff20;
                     buff_class4 = buff_class0;
                     end
                
                end    
            S11: begin
                 if( buff22 > buff21 ) class = buff_class4;
                 else class = buff_class2;
                 
                 finish_accel = 1'b1;
                 end
            S12: begin
                 next_state = S12;
                 rst_stage = 1'b0;
                 end
            default:    next_state = S0;
        endcase
    
    
endmodule