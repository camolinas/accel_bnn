`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06/15/2021 08:46:23 PM
// Design Name: 
// Module Name: mux_dot
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module mux_dot #(parameter width = 8, zero = 8'b0)(
    input [width-1:0] in,                
    input weight,      
    output [width-1:0] y );
                 
    //parameter zero = 8'b0;               
                     
    assign y = weight? in : zero;
    
endmodule