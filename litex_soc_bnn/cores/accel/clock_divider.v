`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05/31/2021 05:48:20 PM
// Design Name: 
// Module Name: clock_divider
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

// k17 -> 125 MHz = 8 ns
// 0.5 s -> 62.5 M  
// 2**27 = ~134 M
// 1.152 MHz -> 108.5 counts
// 1 ms -> 125k counts

module clock_divider(
    input clk,
    input rst,
    input counts,
    
    output reg new_clk
    );
    
    reg [26:0]counter;
    
//    parameter COUNTS = 27'd62500000;       // Period 1s    
    
    always @(posedge clk)
        if(rst) begin
                counter <= 27'd0;
                new_clk <= 1'b1;            
                end
        else if(counter == counts ) begin   
                                    counter <= 27'd0;
                                    new_clk <= ~new_clk;
                                    end
        else counter <= counter + 27'd1;
    
endmodule
