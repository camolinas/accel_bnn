`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06/15/2021 08:45:12 PM
// Design Name: 
// Module Name: conv_dot
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module conv_dot #(parameter width = 72, size = 8)(
    input [width-1:0] slice,     // Slice from Feature map (Input)
    input [8:0]weights,          // Kernel (One Channel)
    
    output [width-1:0] y );
    
    mux_dot mux1( slice[size-1:0],        weights[0], y[size-1:0] );
    mux_dot mux2( slice[size*2-1:size],   weights[1], y[size*2-1:size] );
    mux_dot mux3( slice[size*3-1:size*2], weights[2], y[size*3-1:size*2] );
    mux_dot mux4( slice[size*4-1:size*3], weights[3], y[size*4-1:size*3] );
    mux_dot mux5( slice[size*5-1:size*4], weights[4], y[size*5-1:size*4] );
    mux_dot mux6( slice[size*6-1:size*5], weights[5], y[size*6-1:size*5] );
    mux_dot mux7( slice[size*7-1:size*6], weights[6], y[size*7-1:size*6] );
    mux_dot mux8( slice[size*8-1:size*7], weights[7], y[size*8-1:size*7] );
    mux_dot mux9( slice[size*9-1:size*8], weights[8], y[size*9-1:size*8] );
    
endmodule