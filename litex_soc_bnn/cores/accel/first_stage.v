`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06/15/2021 05:50:01 PM
// Design Name: 
// Module Name: first_stage
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module first_stage(
    input clk,
    input rst,
    
    input load,
    input [9:0]addr_loadImg,
    input [7:0]pixel,
    
    output ready_load,
    output reg finish_inStage,
    output reg [12:0]element
    
    );
    
    reg [3:0]state;
    reg [3:0]next_state;
    reg [3:0]buff_nextState;
    
    // Conv. Module
    reg rst_conv;
    reg enable_conv;  
    
    // RAM Conv. Result Mem.
    reg wen_convMem;
    reg [5:0]addr_convMem;
    reg [11:0]din_convMem;
    reg [5:0]buff_addrconv;
    reg [5:0]buff_addrconv2;
    
    // Max. Pooling Module
    reg [11:0] buff_in0, buff_in1, buff_in2, buff_in3, buff_in4, buff_in5, buff_in6;
    reg [11:0] buff_in7, buff_in8, buff_in9, buff_in10, buff_in11, buff_in12, buff_in13;
    reg [11:0] buff_in14, buff_in15, buff_in16, buff_in17, buff_in18, buff_in19, buff_in20;
    reg [11:0] buff_in21, buff_in22, buff_in23, buff_in24, buff_in25, buff_in26, buff_in27;
    reg rst_max;
    reg cycle_max;
    reg [11:0] max_out0, max_out1, max_out2, max_out3;
    reg [11:0] max_out4, max_out5, max_out6, max_out7;
    reg [11:0] max_out8, max_out9, max_out10, max_out11;
    reg [11:0] max_out12;    
    
    // Batch Module
    reg rst_batch;
    reg enable_batch;
        
    // Max. Pooling Module
    wire [11:0]max0, max1, max2, max3, max4, max5, max6;
    
    // Conv. Module
    wire [11:0]conv_out;
    wire finish_conv;
    wire finish_convOne;
    
    // RAM Conv. Result Mem.
    wire [11:0] inmax0, inmax1, inmax2, inmax3, inmax4, inmax5, inmax6;
    wire [11:0] inmax7, inmax8, inmax9, inmax10, inmax11, inmax12, inmax13;
    wire [11:0] inmax14, inmax15, inmax16, inmax17, inmax18, inmax19, inmax20;
    wire [11:0] inmax21, inmax22, inmax23, inmax24, inmax25, inmax26, inmax27; 
    
    // Batch Module
    wire finish_batch;
    wire finish_allBatch;
    
    // RAM Batch Result
    wire [12:0]batch_out;
    
    parameter S0 = 4'd0;        // Reset
    parameter S1 = 4'd1;        // 
    parameter S2 = 4'd2;        // 
    parameter S3 = 4'd3;        //
    parameter S4 = 4'd4;        //
    parameter S5 = 4'd5;        // 
    parameter S6 = 4'd6;        // 
    parameter S7 = 4'd7;        //
    parameter S8 = 4'd8;        //
    
    parameter S9 = 4'd9; 
    
    
    conv_layer conv(   .clk(clk),
                        .rst(rst_conv),
                        .enable(enable_conv),
                        .load(load),
                        .addr_loadImg(addr_loadImg),
                        .pixel(pixel),
                        .ready_load(ready_load),
                        .finish_conv(finish_conv),
                        .finish_one(finish_convOne), 
                        .conv_out(conv_out)
                        
                    );
    
    
    ram_inmax resultConv(   .clk(clk),
                            .write_enable(wen_convMem),
                            .addr(addr_convMem),
                            .dataIn(din_convMem), 
                            
                            .data1(inmax0), 
                            .data2(inmax1),
                            .data3(inmax2),
                            .data4(inmax3),
                            
                            .data5(inmax4), 
                            .data6(inmax5), 
                            .data7(inmax6), 
                            .data8(inmax7), 
                            
                            .data9(inmax8),
                            .data10(inmax9),
                            .data11(inmax10),
                            .data12(inmax11),
                            
                            .data13(inmax12), 
                            .data14(inmax13), 
                            .data15(inmax14), 
                            .data16(inmax15),
                            
                            .data17(inmax16),
                            .data18(inmax17),
                            .data19(inmax18), 
                            .data20(inmax19),
                            
                            .data21(inmax20),
                            .data22(inmax21),
                            .data23(inmax22),
                            .data24(inmax23),
                            
                            .data25(inmax24), 
                            .data26(inmax25),
                            .data27(inmax26),
                            .data28(inmax27)
                            
                     );

    maxpool_slice maxpool0(clk, rst_max, buff_in0, buff_in1, buff_in2, buff_in3, max0);
    maxpool_slice maxpool1(clk, rst_max, buff_in4, buff_in5, buff_in6, buff_in7, max1);
    maxpool_slice maxpool2(clk, rst_max, buff_in8, buff_in9, buff_in10, buff_in11, max2);
    maxpool_slice maxpool3(clk, rst_max, buff_in12, buff_in13, buff_in14, buff_in15, max3);
    maxpool_slice maxpool4(clk, rst_max, buff_in16, buff_in17, buff_in18, buff_in19, max4);
    maxpool_slice maxpool5(clk, rst_max, buff_in20, buff_in21, buff_in22, buff_in23, max5);
    maxpool_slice maxpool6(clk, rst_max, buff_in24, buff_in25, buff_in26, buff_in27, max6);
    

    batch_layer batch(  .clk(clk),
                        .rst(rst_batch),
                        .enable(enable_batch),
                        .in0(max_out0),
                        .in1(max_out1),
                        .in2(max_out2),
                        .in3(max_out3),
                        .in4(max_out4),
                        .in5(max_out5),
                        .in6(max_out6),
                        .in7(max_out7),
                        .in8(max_out8),
                        .in9(max_out9),
                        .in10(max_out10),
                        .in11(max_out11),
                        .in12(max_out12),
                        .finish_allBatch(finish_allBatch),
                        .finish_batch(finish_batch),
                        .element(batch_out)
                        
                     );

    
    always @(posedge clk)
        if(rst) state <= S0;
        else    state <= next_state;
        
    
    always @(posedge clk)
        case(state)
        S0: begin
            if(load) next_state = S9;
            else    next_state = S1;
            
            finish_inStage = 1'b0;
            
            // Conv Module
            rst_conv = 1'd1;
            enable_conv = 1'd1;
            
            // RAM Conv result
            wen_convMem = 1'd0;
            addr_convMem = 6'd0;
            buff_addrconv = 6'd0;
            
            // MAX. Pooling Modules
            cycle_max =  1'b0;
            rst_max = 1'b1;
            
            // Batch Module
            rst_batch = 1'b1;
            enable_batch = 1'b0; 
            end
        S1: begin
            rst_conv = 1'd0;
            if(finish_conv) begin
                            next_state = S3;
                            enable_conv = 1'b0;
                            end 
            else    next_state = S2;
            
            addr_convMem = buff_addrconv;
            buff_addrconv2 = buff_addrconv;
            
            finish_inStage = 1'b0;
            end
        S2: begin
            wen_convMem = 1'd1; 
                
            if(finish_convOne)  begin
                                next_state = S1;
                                
                                din_convMem = conv_out;
                                buff_addrconv = buff_addrconv2 + 6'd1;                               
                                end
            else    next_state = S2;
            end
        S3: begin                       // Prepare Max. Pooling
            next_state = S4;

            wen_convMem = 1'd0;
            buff_addrconv = 6'd0;
            addr_convMem = 6'd0;            
            end
        S4: begin
            if(cycle_max) next_state = S6;
            else    next_state = S5;
                
            buff_in0 = inmax0; buff_in1 = inmax1; buff_in2 = inmax2; buff_in3 = inmax3; 
            buff_in4 = inmax4; buff_in5 = inmax5; buff_in6 = inmax6; buff_in7 = inmax7;
            buff_in8 = inmax8; buff_in9 = inmax9; buff_in10 = inmax10; buff_in11 = inmax11; 
            buff_in12 = inmax12; buff_in13 = inmax13; buff_in14 = inmax14; buff_in15 = inmax15;
            buff_in16 = inmax16; buff_in17 = inmax17; buff_in18 = inmax18; buff_in19 = inmax19; 
            buff_in20 = inmax20; buff_in21 = inmax21; buff_in22 = inmax22; buff_in23 = inmax23;
            buff_in24 = inmax24; buff_in25 = inmax25; buff_in26 = inmax26; buff_in27 = inmax27;  
            
            rst_max = 1'b0;
            end
        S5: begin
            next_state = S4;
            cycle_max =  1'b1;
            
            rst_max = 1'b1;    
            max_out0 = max0;
            max_out1 = max1;
            max_out2 = max2;
            max_out3 = max3;
            max_out4 = max4;
            max_out5 = max5;
            max_out6 = max6;
                
            addr_convMem = 6'd14;
            end
        S6: begin
            next_state = S7;
        
            max_out7 = max0;
            max_out8 = max1;
            max_out9 = max2;
            max_out10 = max3;
            max_out11 = max4;
            max_out12 = max5;
            
            cycle_max =  1'b0;
            
            rst_batch = 1'b0;
            enable_batch = 1'b1;
            end   
        S7: begin
            if(finish_batch) begin
                             next_state = S8;
                             enable_batch = 1'b0;
                             end
            else    next_state = S7;
            
            rst_max = 1'b1;
            
            finish_inStage = 1'b0;
            end
        S8: begin
            if(finish_allBatch) next_state = S8;
            else    begin
                    next_state = S1;
                    enable_conv = 1'b1;
                    end
            
            element = batch_out;
            finish_inStage = 1'b1;
            end    
        S9: begin
            next_state = S9;
            rst_conv = 1'd0;
            end 
        default next_state = S0;
        endcase
        

endmodule