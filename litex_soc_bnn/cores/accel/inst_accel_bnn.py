#
# Migen Instance to incorporate the Accelerator BNN verilog Modle 
#

import os

from migen import *

from litex.soc.interconnect.csr import *
from litex.soc.integration.doc  import AutoDoc, ModuleDoc


# Helpers ------------------------------------------------------------------------------------------
def _to_signal(obj):
  return obj.raw_bits() if isinstance(obj, Record) else obj


# Accel bnn ------------------------------------------------------------------------------
class ACCEL_BNN(Module, AutoCSR, AutoDoc):

  def __init__(self, platform, pads, pin_time):
    self.intro   = ModuleDoc("Accel BNN instance from verilog module");
    self.rst  = CSRStorage(1, reset=0x1, name="rst", description="""rst: Reset Signal.""");
    self.select  = CSRStorage(1, reset=0x0, name="select", description="""Select: Signal to avoid load image step.""");
    self.r_Rx  = CSRStorage(8, reset=0x00, name="r_Rx", description="""r_Rx: pixel input image.""");
    self.rx_ok  = CSRStorage(1, reset=0x0, name="rx_ok", description="""rx_ok: Ready to load pixel image.""");
    
    
    self.prediction = CSRStatus(4, reset=0x00, name="prediction", description="""prediction: Output with the class number prediction.""");    
    self.display_out = _to_signal(pads)
    
    self.receive_ok = CSRStatus(1, reset=0x0, name="receive_ok", description="""receive_ok: """);
    self.check_led = CSRStatus(1, reset=0x0, name="check_led", description="""check_led: """);
    self.check_init = _to_signal(pin_time)
    

    # Parameters
    self.params = dict(

      # CLK
      i_clk = ClockSignal("sys"),

      # Inputs
      i_rst = self.rst.storage,
      i_select = self.select.storage,
      i_r_Rx = self.r_Rx.storage,
      i_rx_ok = self.rx_ok.storage,

      # Outputs
      o_prediction = self.prediction.status,
      o_display_out = self.display_out,

      o_receive_ok = self.receive_ok.status,
      o_check_led = self.check_led.status,
      o_check_init = self.check_init
      
    );

    # Add sources.
    self.add_sources(platform);


  @staticmethod
  def add_sources(platform):
    sources = [

      # Core.
      "top_bnn.v",
      # "clock_divider.v",

      "accel_bnn.v",

      "first_stage.v",

      "conv_layer.v",
      "ram_img.v",
      "rom_Wconv.v",
      "conv_oneStep.v",
      "conv_dot.v",
      "mux_dot.v",
      "conv_sum.v",

      "ram_inmax.v",
      "maxpool_slice.v",

      "batch_layer.v",
      "rom_wbatch.v",
      "sign_ineq.v",
      "ram_resultBatch.v",

      "dense_layer.v",
      "rom_wdense.v",

      "deco_display.v"

       ];

    # Add sources.
    platform.add_sources("cores/accel/", *sources)


  def do_finalize(self):
    self.specials += Instance("top_bnn", **self.params);
