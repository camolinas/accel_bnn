`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06/15/2021 08:47:54 PM
// Design Name: 
// Module Name: rom_wbatch
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module rom_wbatch #(parameter width = 12)(
    input [10:0]addr,
    
    output [width-1:0] data0,
    output [width-1:0] data1,
    output [width-1:0] data2,
    output [width-1:0] data3,
    output [width-1:0] data4,
    output [width-1:0] data5,
    output [width-1:0] data6
    );
    
    reg [width-1:0] mem [2027:0]; 
    
    initial
     begin        
        $readmemb("/home/camilo/Documents/accelerator_BNN/code_test_acc/batchW_alpha2048_sort.mem", mem);
     end
     
    assign data0  = mem[addr];
    assign data1  = mem[addr+1];
    assign data2  = mem[addr+2];
    assign data3  = mem[addr+3];
    assign data4  = mem[addr+4];
    assign data5  = mem[addr+5];
    assign data6  = mem[addr+6];    
    
endmodule