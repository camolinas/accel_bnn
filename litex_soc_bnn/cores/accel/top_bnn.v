`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07/07/2021 05:02:53 PM
// Design Name: 
// Module Name: top_serialTime
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

// k17 -> 125 MHz = 8 ns


module top_bnn(

    input clk,
    input rst,
    
    input select,
    
    input [7:0]r_Rx,
    input rx_ok,
    
    output [3:0]prediction,
    output [6:0]display_out,
    
    output receive_ok,
    // Testing signals
    output check_led,
    // output [7:0]check_pixel
    output reg check_init
    
    );
    
    reg [3:0]state = 0;
    reg [3:0]next_state = 0;
    
    // Accel BNN
    reg rst_accel;
    reg load;
    reg [7:0]pixel;
    reg [9:0]addr_loadImg;
    reg [3:0]buff_class;
    
    // Img. Addressing 
    reg [9:0]buff_addrImg;
    reg [9:0]buff_addrImg2;
    
    wire [3:0]class;
    wire finish_accel;
    wire ready_load;

    // Clock Divider
    // reg rst_clk2;
    // wire clk2;

    // Valid State - Load Img
    reg sample_rx;
    reg rx_ok_valid;
    reg receive;
    
    parameter c_CLKS_PER_BIT = 11'd1085;
    
    parameter S0 = 4'd0;
    parameter S1 = 4'd1;
    parameter S2 = 4'd2;
    parameter S3 = 4'd3;
    parameter S4 = 4'd4;
    parameter S5 = 4'd5;
    parameter S6 = 4'd6;
    parameter S7 = 4'd7;
    parameter S8 = 4'd8;
    
    // Auxiliar LEDs
    reg led = 0;
    
    // clock_divider clk_load( .clk( clk ),
    //                         .rst( rst_clk2 ),
    //                         .counts( counts ),

    //                         .new_clk( clk2 )
    //                     );
    
    accel_bnn accel(    .clk( clk ),
                        .rst( rst_accel ),
                        .load( load ),
                        .addr_loadImg( addr_loadImg ),
                        .pixel( pixel ),
                        .ready_load( ready_load ),
                        .finish_accel( finish_accel ),
                        .class( class )
                        
                  );                  
                            
    
    deco_display display(   .number( buff_class ),
                            .display( display_out )
                        );
     
    
    always @(posedge clk)
        if(rst) state <= S0;
        else    state <= next_state;
    

    always @(posedge clk)
        case(state)
            S0: begin
                if(select) next_state <= S4;
                else    next_state <= S1;
                
                rst_accel <= 1'b1;
                load <= 1'b1;
                
                // Addr Load Img.
                addr_loadImg <= 10'd0;
                buff_addrImg <= 10'd0;


                check_init <= 1'd0;
                led <= 1'b0;
                rx_ok_valid <= 1'd0;
                receive <= 1'd0;
                
                buff_class <= 4'd10;
                end
            S1: begin                
                // if( rx_ok_valid && ready_load )  next_state <= S2;
                if( rx_ok_valid && ready_load && rx_ok )  next_state <= S2;  
                else next_state <= S8;
                
                // sample_rx <= rx_ok;

                rst_accel <= 1'b0;
                
                addr_loadImg <= buff_addrImg;
                buff_addrImg2 <= buff_addrImg;
                end
            S8: begin
                next_state <= S1;

                // if( (sample_rx == 1'd0) && (rx_ok == 1'd1) ) begin
                // if( sample_rx == 1'd0) begin
                if( rx_ok == 1'd0) begin
                    rx_ok_valid <= 1'd1;
                end

                receive <= 1'd1;

                end
            S2: begin                
                if(addr_loadImg == 10'd783) next_state <= S3;
                else    next_state <= S1;

                pixel <= r_Rx;
                buff_addrImg <= buff_addrImg2 + 10'd1;

                rx_ok_valid <= 1'd0;
                receive <= 1'd0;
                end  
            S3: begin
                next_state <= S4;
                rx_ok_valid <= 1'd0;
                rst_accel <= 1'b1;
                load <= 1'b0; 
                end
            S4: begin
                next_state <= S5;
                
                rst_accel <= 1'b1;
                load <= 1'b0;
                
                // Addr Load Img.
                addr_loadImg <= 10'd0;
                buff_addrImg <= 10'd0;
                end
            S5: begin
                if(finish_accel) next_state <= S6;
                else next_state <= S5;
                
                rst_accel <= 1'b0;
                
                check_init <= 1'd1;
                led <= 1'b1;
                end
            S6: begin
                buff_class <= class;
                check_init <= 1'd0;

                // prediction <= class;
                end
            default: next_state <= S0;
        endcase        
        

    assign prediction = buff_class;
    assign receive_ok = receive;

    // Testing LEDs
    assign check_led = led;
    // assign check_pixel = pixel;
    
endmodule