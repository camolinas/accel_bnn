`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06/15/2021 08:47:09 PM
// Design Name: 
// Module Name: conv_sum
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module conv_sum #(parameter width = 72, size = 8)(
    input [width-1:0] x,
    output [11:0] y
    );
    
    assign y =  x[size*9-1:size*8] + x[size*8-1:size*7] + // 9th+8th | e.g 17:16 15:14 
                x[size*7-1:size*6] + x[size*6-1:size*5] + // 7th+6th | e.g 13:12 11:10
                x[size*5-1:size*4] + x[size*4-1:size*3] + // 5th+4th | e.g 9:8 7:6 
                x[size*3-1:size*2] + x[size*2-1:size] +   // 3dh+2nd | e.g 5:4 3:2 
                x[size-1:0];
    
endmodule