#ifndef __PERIPHERALS_H
#define __PERIPHERALS_H


/*** led_test             ***/
void led_test(void);

/*** tm_test              ***/
void tm_test(void);

/*** verilog_ram_mem              ***/
void v_ramWrite(uint8_t data_in, int addr_mem);
uint8_t v_ramRead(int addr_mem);

/*** verilog_accel_bnn              ***/
void accel_rst(void);

uint8_t accel_prediction(uint8_t select);


#endif
