#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <irq.h>
#include <uart.h>
#include <console.h>
#include <generated/csr.h>

#include "../include/peripherals.h"



static void reboot(void)
{
	ctrl_reset_write(1);
}

int main(void){

	uint8_t data_out = 0;
	uint8_t read_data = 0;
	int count = 0;

#ifdef CONFIG_CPU_HAS_INTERRUPT
	irq_setmask(0);
	irq_setie(1);
#endif
	uart_init();

	puts("\nAccelSoC BNN - CPU testing software built "__DATE__" "__TIME__"\n");	

	//for(int j = 0; j < 5; j++) v_ramWrite(0, j);
	//v_ramWrite(0, 781);
	//v_ramWrite(0, 782);
	
	accel_rst();
	//data_out = accel_prediction(1);
	
	busy_wait(1000);
	//if( accel_bnn_check_led_read() ) printf("\nOk prediction Step ...\n");
	
	//led_test();
	for(int j = 0; j < 100; j++){

		printf("\n--- New Image --- \n");
		// busy_wait(3000);
		while( read_data != 255 ){
			read_data = readchar();
		}
		//printf("\nInit Data: %d \n", read_data);
		read_data = readchar();
		printf("\nInput Image: %d \n", read_data);

		while( count != 783 ) {	
			read_data = readchar();
			//printf("Data %d in: %d \n", count, read_data);
			
			v_ramWrite(read_data, count);
			count = count + 1;
			if( count == 600) accel_rst();
		}
		count = 0;

		accel_rst();
		data_out = accel_prediction(0);

		//printf("\nAceel Prediciton is: %d \n", data_out);
		//busy_wait(3000);
		//if( accel_bnn_check_led_read() ) printf("\nOk prediction Step ...\n");
	}
	
	led_test();

	return 0;
}
