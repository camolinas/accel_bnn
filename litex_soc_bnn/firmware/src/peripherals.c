#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <irq.h>
#include <uart.h>
#include <console.h>
#include <generated/csr.h>
#include <hw/common.h> 

#include "../include/peripherals.h" 



/*** led_test             ***/
void led_test(void){
  int i;
  // printf("led_test...\n");
  for(i=0; i<0x10; i++){
    leds_out_write(i);
    busy_wait(500); };                                     // 500 ms
  leds_out_write(0x0);
};


/*** verilog_ram              ***/
void v_ramWrite(uint8_t data_in, int addr_mem){

  v_ram_addr_write(addr_mem);
  v_ram_write_enable_write(1);
  v_ram_dataIn_write(data_in);
  v_ram_write_enable_write(0);
}

uint8_t v_ramRead(int addr_mem){
  uint8_t out;

  v_ram_write_enable_write(0);
  v_ram_addr_write(addr_mem);
  out = v_ram_dataOut_read();
  // printf("Mem read Data: %d \n", out);

  return out;
}


/*** accel bnn              ***/
void accel_rst(void){
  accel_bnn_rst_write(1);
}

uint8_t accel_prediction(uint8_t select){

  uint8_t data_out = 0;
  uint8_t class = 0;

  accel_bnn_rst_write(1);
  accel_bnn_select_write(select);
  accel_bnn_rx_ok_write(0);
  accel_bnn_rst_write(0);

  if( !select ){
    // printf("\n*** I'm loading Img ***\n");

    for(int i = 0; i < 784; i++){

      accel_bnn_rx_ok_write(0);
      // busy_wait(1);

      data_out = v_ramRead( i );
      accel_bnn_r_Rx_write(data_out);
      while( !accel_bnn_receive_ok_read() );
      accel_bnn_rx_ok_write(1);
      
    }
  }

  class = accel_bnn_prediction_read();
  return class;
}


/*** tm_test              ***/
void tm_test(void){
  unsigned int ms=1000;
  printf("tm_test...\n");

  /* Init timer */
  timer0_en_write(0);
  timer0_reload_write(0);
  timer0_load_write(CONFIG_CLOCK_FREQUENCY/1000*ms);
  timer0_en_write(1);
  timer0_update_value_write(1);

  leds_out_write(0xF);
  while(timer0_value_read()) timer0_update_value_write(1);
  leds_out_write(0x0);
};