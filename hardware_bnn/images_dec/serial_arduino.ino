#include <SoftwareSerial.h>

SoftwareSerial uart2(11, 10); // RX | TX

int count = 0;
uint8_t data = 0;

void setup() {

  Serial.begin(115200);
  uart2.begin(115200);

}

void loop(){
  
    while( uart2.available() ){ 
      //buff[count] = uart2.read();
      Serial.print(count);
      Serial.print("  ");
      data = uart2.read();
      Serial.println(data);
      count++;
      uart2.write(data);
    }  
    
}
