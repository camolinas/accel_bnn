`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06/15/2021 08:56:53 PM
// Design Name: 
// Module Name: testbench_topSerial
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module testbench_topSerial();
    reg clk, rst; 
    reg select;
    
    reg r_Rx_Serial;
    wire o_Tx_Serial;
    
    wire check_rst;
    wire check_clk;
    
    wire check_rx_ok;
    wire check_tx_ok;
    
    wire [2:0]state;
//    wire [9:0]addrLoadRom;
    wire [7:0]pixel;
    wire [7:0]pixelIn;
    
    wire ok;
    wire [3:0]stateAccel;
    
//    // First Stage
//     Conv.
    wire [5:0]addrInmax;
    wire [11:0]inmax0; 
    wire [11:0] inmax1, inmax2, inmax3, inmax4, inmax5, inmax6, inmax7;
    wire [71:0]convSlice;
    wire [8:0]convW;
    wire [4:0]colm;
    wire [4:0]row;
    wire [9:0]convAddr;
    wire [2:0]convState;
    wire [3:0]convChan;
    wire [11:0]conv;
    wire ok_conv;
    wire [5:0]convWait;
//    // Max. Pooling
    wire [3:0]stateStage;
    wire [11:0]max0;
    wire [11:0]max1;
    wire [11:0]max2;
    wire [11:0]max3;
    wire [11:0]max4;
    wire [11:0]max5;
    wire [11:0]max6;
    wire [11:0]max7;
    wire [11:0]max8;
    wire [11:0]max9;
    wire [11:0]max10;
    wire [11:0]max11;
    wire [11:0]max12;
    
    // Batch
    wire [2:0]stateBatch;
    wire [10:0]addrBatchW;
    wire [12:0]elBatch;
    wire weBatchMem;
    wire [12:0]resultBatch;
    
    // Dense
    wire [2:0]stateDense;
    wire [7:0]addrInDense;
    wire [9:0]addrWDense;
    wire [38:0]wDense;
    wire [38:0]andDense; 
    wire [8:0]sumDense;
    wire one_dense;
    wire [8:0]class0; 
    wire [8:0]class1; 
    wire [8:0]class2; 
    wire [8:0]class3; 
    wire [8:0]class4; 
    wire [8:0]class5; 
    wire [8:0]class6; 
    wire [8:0]class7;
    wire [8:0]class8; 
    wire [8:0]class9;
    wire okDense;
    
    wire [3:0]class;
    wire [6:0]display;
    

      topSerial_sim top(    .clk( clk ),
                            .rst( rst ),
                            .select( select ),
                            
                            .r_Rx_Serial( r_Rx_Serial ),
                            .o_Tx_Serial( o_Tx_Serial ),
                            
                            .display_out( display ),
                            
                            .check_led(),
                            .check_clk( check_clk ),
                            .check_rst( check_rst ),
                            .check_init(),
                            .check_rx_ok( check_rx_ok ),
                            .check_tx_Done( check_tx_ok ),
        
                            .check_state( state ),
    //                        .check_addrLoadRom,
                            .check_pixelIn( pixelIn ),
                            .check_pixel( pixel ),
        
                            .check_okAccel( ok ),
                            .check_class( class ),
          
                            .check_stateAccel( stateAccel ),
        
    ////    // Test Signals First Stage
                            .check_addrInmax( addrInmax ),
                            .check_inmax0( inmax0 ), 
                            .check_inmax1( inmax1 ), 
                            .check_inmax2( inmax2 ), 
                            .check_inmax3( inmax3 ), 
                            .check_inmax4( inmax4 ), 
                            .check_inmax5( inmax5 ), 
                            .check_inmax6( inmax6 ), 
                            .check_inmax7( inmax7 ),
        
                            .check_convSlice( convSlice ),
                            .check_convW( convW ),
                            .check_colm( colm ),
                            .check_row( row ),
                            .check_convAddr( convAddr ),
                            .check_convState( convState ),
                            .check_convChan( convChan ),
                            .check_conv( conv ),
                            .check_okConv( ok_conv ), 
                            .check_convWait( convWait ),
                            .check_stateStage( stateStage ),
                            .check_max0( max0 ), .check_max1( max1 ), .check_max2( max2 ), .check_max3( max3 ), .check_max4( max4 ), 
                            .check_max5( max5 ),
                            .check_max6( max6 ), 
                            .check_max7( max7 ), .check_max8( max8 ), .check_max9( max9 ), .check_max10( max10 ), .check_max11( max11 ), .check_max12( max12 ),
        
    ////    // Batch Module
                            .check_stateBatch( stateBatch ),
                            .check_addrBatchW( addrBatchW ),
                            .check_elBatch( elBatch ),
                            .check_weBatchMem( weBatchMem ),
                            .check_resultBatch( resultBatch ),
        
    //    // Dense
                            .check_denseState( stateDense ),
                            .check_okOneDense( one_dense ),
                            .check_addrInDense( addrInDense ),
                            .check_denseAddr( addrWDense ),
                            .check_wDense( wDense ),
                            .check_andDense( andDense ), 
                            .check_sumDense( sumDense ),
                            .check_class0( class0 ), 
                            .check_class1( class1 ), 
                            .check_class2( class2 ), 
                            .check_class3( class3 ), 
                            .check_class4( class4 ), 
                            .check_class5( class5 ), 
                            .check_class6( class6 ), 
                            .check_class7( class7 ),
                            .check_class8( class8 ), 
                            .check_class9( class9 ),
                            .check_okDense( okDense )
                   );
    

        
    always
     begin
        clk = 1; #4; clk = 0; #4;
     end
    
    parameter c_BIT_PERIOD = 8680;
    parameter c_CLKS_PER_BIT = 1085;
    
    reg Tx_DV;
    reg [7:0]Tx_Byte;
    wire Tx_Serial;
    wire Tx_Done;
  
      uart_tx #(.CLKS_PER_BIT(c_CLKS_PER_BIT)) uart_input
    (.i_Clock( clk ),
     .i_Tx_DV( Tx_DV ),
     .i_Tx_Byte( Tx_Byte ),
     .o_Tx_Active(),
     .o_Tx_Serial( Tx_Serial ),
     .o_Tx_Done( Tx_Done )
     );
    
    //always @(posedge clk)    
    //    if(weBatchMem) $display(elBatch);
     
    reg [7:0] img [783:0];
    reg [9:0]index;
    reg [7:0]buff_pixel;
    reg buff_txOk;
     
    initial
     begin
//        $readmemb("/home/camilo/Documents/accelerator_BNN/code_test_acc/img28x28_n2_test.mem", img);
//        $readmemb("/home/camilo/Documents/accelerator_BNN/code_test_acc/img28x28_n0_bin.mem", img);
//        $readmemb("/home/camilo/Documents/accelerator_BNN/code_test_acc/img28x28_n2_refbin.mem", img);
//        $readmemb("/home/camilo/Documents/accelerator_BNN/code_test_acc/images_bin/img28x28_n9_bin.txt", img);
        $readmemb("/home/camilo/Documents/accelerator_BNN/code_test_acc/images_bin/img28x28_n3_bin.txt", img);
        
        index = 1;
        Tx_DV = 1'b0;
        Tx_Byte = 8'd4;

        buff_txOk = 1'd0;
        rst = 1'b1; 
        select = 1'b0;  
        #8;
        
        rst = 1'b0;
        Tx_DV = 1'b1; 

        $display("-------------------- START --------------------");
     end
     
    always @(posedge check_tx_ok)
     begin
           buff_txOk = 1'd1;
     end
     

    always @(posedge clk) 
     begin
        if(buff_txOk) begin
                        index = index + 1;
                        Tx_DV = 1'b1;
                        Tx_Byte = buff_pixel;
                        buff_txOk = 1'd0;
                        end     
        r_Rx_Serial = Tx_Serial;        
     end
     

    always @(negedge clk)
     begin
        if(buff_txOk) begin
                        buff_pixel  = img[index];
                        end
     end
 
endmodule