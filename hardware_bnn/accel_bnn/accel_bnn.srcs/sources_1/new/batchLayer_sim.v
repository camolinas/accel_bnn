`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06/15/2021 08:21:27 PM
// Design Name: 
// Module Name: batchLayer_sim
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module batchLayer_sim(
    input clk,
    input rst,
    input enable,
    input [11:0]in0, [11:0]in1, [11:0]in2, [11:0]in3, [11:0]in4, [11:0]in5, [11:0]in6, 
    input [11:0]in7, [11:0]in8, [11:0]in9, [11:0]in10, [11:0]in11, [11:0]in12,
    
    output reg finish_allBatch,
    output reg finish_batch,
    output reg [12:0]element,
    
    // Test Signals
    output [10:0]check_addr,
    output [11:0]check_alp0, [11:0]check_alp1, [11:0]check_alp2, [11:0]check_alp3, [11:0]check_alp4, [11:0]check_alp5, [11:0]check_alp6,  
    output [12:0]check_element,
    
    output [2:0]check_state
    );
    
    reg [2:0]state;
    reg [2:0]next_state;
    reg cycle;
    
    // ROM Weights
    reg [10:0]addr;
    reg [10:0]buff_addr;
    reg [10:0]buff_addr2;
    
    reg [6:0]buff_element;
    
    // Sing Modules
    reg rst_signs;
    reg [11:0] buff_in0, buff_in1, buff_in2, buff_in3, buff_in4, buff_in5, buff_in6;
    reg [11:0] alpha0, alpha1, alpha2, alpha3, alpha4, alpha5, alpha6;

    // ROM Weights
    wire [11:0] mem0, mem1, mem2, mem3, mem4, mem5, mem6;
    
    // Sing Modules
    wire act0, act1, act2, act3, act4, act5, act6;

    
    parameter S0 = 3'd0;        // Reset
    parameter S1 = 3'd1;        // IDLE
    parameter S2 = 3'd2;        // Calculate
    parameter S3 = 3'd3;        // Load sixth LSB to the result
    parameter S4 = 3'd4;        // Save
    
    parameter S5 = 3'd5;       
    parameter S6 = 3'd6;
    parameter S7 = 3'd7;
    reg [10:0]add_value;      
    
    
    rom_wbatch rom_bw(addr, mem0, mem1, mem2, mem3, mem4, mem5, mem6);
    sign_ineq sign0(clk, rst_signs, buff_in0, alpha0, act0);
    sign_ineq sign1(clk, rst_signs, buff_in1, alpha1, act1);
    sign_ineq sign2(clk, rst_signs, buff_in2, alpha2, act2);
    sign_ineq sign3(clk, rst_signs, buff_in3, alpha3, act3);
    sign_ineq sign4(clk, rst_signs, buff_in4, alpha4, act4);
    sign_ineq sign5(clk, rst_signs, buff_in5, alpha5, act5);
    sign_ineq sign6(clk, rst_signs, buff_in6, alpha6, act6);


    always @(posedge clk)
        if(rst) state <= S0;
        else    state <= next_state;


    always @(posedge clk) 
        case(state)
            S0: begin                                   // Reset
                next_state = S1;
                cycle = 1'b0;
                finish_batch = 1'b0;
                finish_allBatch = 1'b0;
                
                // ROM Weights
                addr = 11'b0;
                buff_addr = 11'b0;
                buff_addr2 = 11'b0;
                
                // Sign_ineq Modules
                rst_signs = 1'b1;
                end
            S1: begin                                   // IDLE and Load Weights
                if(enable) begin
                           if(cycle) next_state = S5;
                           else    next_state = S2;
                           end
                else    next_state = S1;      
                                          
                alpha0 = mem0; alpha1 = mem1; alpha2 = mem2; alpha3 = mem3; alpha4 = mem4; alpha5 = mem5; alpha6 = mem6;
                
                finish_batch = 1'b0;
                end
            S2: begin                                  // Load LSB bits - Input                   
                next_state = S3;
                buff_in0 = in0; buff_in1 = in1; buff_in2 = in2; buff_in3 = in3; buff_in4 = in4; buff_in5 = in5; buff_in6 = in6;
                
                rst_signs = 1'b0;
                add_value = 11'd7;
                end
            S3: begin                                   // Start 
                next_state = S4;
                                        
                buff_addr = buff_addr2 + add_value;                
                end
            S4: begin                                   //  Save the result
                next_state = S1;                        // Return to Load last Weights (MSB)
                cycle =  1'b1;
                
                buff_element = {act6, act5, act4, act3, act2, act1, act0};
                addr = buff_addr;
                buff_addr2 = buff_addr;
                
                rst_signs = 1'b1;
                end    
            S5: begin                                   // Load MSB bits - Input
                next_state = S6;
                buff_in0 = in7; buff_in1 = in8; buff_in2 = in9; buff_in3 = in10; buff_in4 = in11; buff_in5 = in12;
                
                rst_signs = 1'b0;
                add_value = 11'd6;
                end
            S6: begin                                   // Save Output
                next_state = S7;
                
                buff_addr = buff_addr2 + add_value;
                end
            S7: begin
                if(buff_addr == 11'd2028) begin
                                          next_state = S7;
                                          finish_allBatch = 1'b1;
                                          end
                else next_state = S1;
                cycle =  1'b0;
                finish_batch = 1'b1;
                
                addr = buff_addr;
                buff_addr2 = buff_addr;
                
                element[6:0] = buff_element;
                element[12:7] = {act5, act4, act3, act2, act1, act0};
                                
                rst_signs = 1'b1;
                end
            default: next_state = S0;
        endcase
    
      
    assign check_addr = addr;
    
//    assign check_alp0 = alpha0;
//    assign check_alp1 = alpha1;
//    assign check_alp2 = alpha2;
//    assign check_alp3 = alpha3;
//    assign check_alp4 = alpha4;
//    assign check_alp5 = alpha5;
//    assign check_alp6 = alpha6;
//    assign check_element = element; 
    
    assign check_state = state;
    
endmodule