`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06/15/2021 08:14:29 PM
// Design Name: 
// Module Name: ramInmax_sim
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ramInmax_sim #(parameter width = 12, COLMS = 5'd26)(
    input clk,
    input write_enable,
    input [5:0]addr,
    input [width-1:0]dataIn,
    
    output [width-1:0] data1,
    output [width-1:0] data2,
    output [width-1:0] data3,
    output [width-1:0] data4,
    
    output [width-1:0] data5,
    output [width-1:0] data6,
    output [width-1:0] data7,
    output [width-1:0] data8,
    
    output [width-1:0] data9,
    output [width-1:0] data10,
    output [width-1:0] data11,
    output [width-1:0] data12,
    
    output [width-1:0] data13,
    output [width-1:0] data14,
    output [width-1:0] data15,
    output [width-1:0] data16,
    
    output [width-1:0] data17,
    output [width-1:0] data18,
    output [width-1:0] data19,
    output [width-1:0] data20,
    
    output [width-1:0] data21,
    output [width-1:0] data22,
    output [width-1:0] data23,
    output [width-1:0] data24,
    
    output [width-1:0] data25,
    output [width-1:0] data26,
    output [width-1:0] data27,
    output [width-1:0] data28,
    
    // Test signals
    //output [5:0] check_addr
    output [width-1:0]check_dataIn
    );
    
    reg [width-1:0] mem [51:0];
    
     
    always @(posedge clk)
        if(write_enable) mem[addr] <= dataIn;

  
    assign data1 = mem[addr];               // First row
    assign data2 = mem[addr+1];
    assign data3 = mem[addr+COLMS];         // Second row
    assign data4 = mem[addr+COLMS+1];       
    
    assign data5 = mem[addr+2];             // First row
    assign data6 = mem[addr+2+1];       
    assign data7 = mem[addr+2+COLMS];       // Second row
    assign data8 = mem[addr+2+COLMS+1];
    
    assign data9 = mem[addr+4];               // First row
    assign data10 = mem[addr+4+1];
    assign data11 = mem[addr+4+COLMS];         // Second row
    assign data12 = mem[addr+4+COLMS+1];       
    
    assign data13 = mem[addr+6];             // First row
    assign data14 = mem[addr+6+1];       
    assign data15 = mem[addr+6+COLMS];       // Second row
    assign data16 = mem[addr+6+COLMS+1];
    
    assign data17 = mem[addr+8];               // First row
    assign data18 = mem[addr+8+1];
    assign data19 = mem[addr+8+COLMS];         // Second row
    assign data20 = mem[addr+8+COLMS+1];       
    
    assign data21 = mem[addr+10];             // First row
    assign data22 = mem[addr+10+1];       
    assign data23 = mem[addr+10+COLMS];       // Second row
    assign data24 = mem[addr+10+COLMS+1];     
    
    assign data25 = mem[addr+12];               // First row
    assign data26 = mem[addr+12+1];
    assign data27 = mem[addr+12+COLMS];         // Second row
    assign data28 = mem[addr+12+COLMS+1];       
              

//    assign check_addr = addr;
    assign check_dataIn = dataIn;
endmodule     