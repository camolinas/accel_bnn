`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06/15/2021 06:03:16 PM
// Design Name: 
// Module Name: ramResultBatch_sim
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ramResultBatch_sim(
    input clk,
    input write_enable,
    input [7:0]addr,
    input [12:0]dataIn, 
     
    output [12:0]data1,
    output [12:0]data2,
    output [12:0]data3
    
    // Test signals
//    output [9:0] check_addr
    );  
    
    reg [12:0] mem [155:0];  // 13x13x12 / 13 = 156
         
    always @(posedge clk)
        if(write_enable) mem[addr]   <= dataIn;                          


    assign data1 = mem[addr];
    assign data2 = mem[addr+1];
    assign data3 = mem[addr+2];              
    
//    assign check_addr = addr;
endmodule