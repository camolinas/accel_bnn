`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06/15/2021 08:19:18 PM
// Design Name: 
// Module Name: maxpool_slice1
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module maxpool_slice1 #(parameter width = 4'd12)(
    input clk,
    input rst,
    
    input [width-1:0]slice0,
    input [width-1:0]slice1,
    input [width-1:0]slice2,
    input [width-1:0]slice3,
    
    output reg [11:0]element
    
    // Test Signals
    //output [size-1:0]check_buff1,
    //output [size-1:0]check_buff2,
    //output [1:0]check_state
    );
    
    reg [1:0]state = 2'b0;
    
    reg [width-1:0]buff1;
    reg [width-1:0]buff2;
    
    localparam S0 = 2'd0;
    localparam S1 = 2'd1;


    always @(posedge clk)
        case(state)
            S0: begin
                if(rst) state <= S0;
                else state <= S1;
                
                if( slice1 > slice0 ) buff1 <= slice1;
                else buff1 <= slice0;
                
                if( slice3 > slice2 ) buff2 <= slice3;
                else buff2 <= slice2;
                end
            S1: begin
                if(rst) state <= S0;
                else state <= S1;
                
                  if( buff2 > buff1 ) element <= buff2;
                  else element <= buff1;
              
                end
            default: state <= S0; 
        endcase


//    assign check_buff1 = buff1;
//    assign check_buff2 = buff2;
//    assign check_state = state;
            
endmodule