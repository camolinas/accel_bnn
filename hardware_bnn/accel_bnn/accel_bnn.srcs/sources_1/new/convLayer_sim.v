`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06/15/2021 06:48:56 PM
// Design Name: 
// Module Name: convLayer_sim
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module convLayer_sim #(parameter width = 72, size = 8)(
    input clk,
    input rst,
    input enable,
    
    input load,
    input [9:0]addr_loadImg,
    input [7:0]pixel,
    
    output reg ready_load,
    output reg finish_conv,
    output reg finish_one,
    output reg [11:0]conv_out,
    
    output [7:0]check_pixel,
    
    // TEST SIGNALS
    
//  // Conv one Step Module
    output check_rstStep,
    output [width-1:0]check_sliceLoad,
    output [width-1:0]check_sliceIn,
    output [8:0]check_wLoad,
    output [width-1:0]check_dot,
    output [11:0]check_sum,
    
//    // Counter Image Module
    output [4:0]check_colm,
    output [4:0]check_row,
    output [9:0]check_addImgIn,
    
//    //General
    output [2:0]check_state,
    output [3:0]check_addChan,
    output [5:0]check_waitCounter
    );
    
    reg [2:0]state;
    reg [2:0]next_state;
    reg [2:0]buff_nextState;
    
    reg [5:0]wait_counter;
    reg [5:0]wait_counter2;
    
    // Conv One Step Module
    reg [width-1:0]slice;
    reg [8:0]kernel;
    reg rst_slice;
    
    // Adressing Image Module
    reg [4:0]colm;
    reg [4:0]row;
    reg [4:0]buff_colm;
    reg [4:0]buff_row;
    reg [9:0]addr_img;
    
    // Ram Image
    reg wen_ramImg;
    reg [9:0]addr_ramImg = 0;
    reg [size-1:0]dataIn_ramImg = 0;
    
    // Rom Weights 
    reg [3:0]addr_w;
    reg [3:0]buff_addrw2;
    reg [3:0]buff_addrw;
    
    
    // Conv One Step Module
    wire finish_slice;
    wire [11:0]element;

    // Ram Image
    wire [size-1:0]d_img1;
    wire [size-1:0]d_img2;
    wire [size-1:0]d_img3;
    wire [size-1:0]d_img4;
    wire [size-1:0]d_img5;
    wire [size-1:0]d_img6;
    wire [size-1:0]d_img7;
    wire [size-1:0]d_img8;
    wire [size-1:0]d_img9;
    
    // Rom Weights 
    wire [8:0]conv_weights;       
        
    
    localparam S0 = 3'd0;
    localparam S1 = 3'd1;
    localparam S2 = 3'd2;
    localparam S3 = 3'd3;
    localparam S4 = 3'd4;
    
    localparam S5 = 3'd5;
    localparam S6 = 3'd6;
    localparam S7 = 3'd7;
    
    localparam CHANNELS = 4'd12;             // Number of Kernel channels
    localparam LASTADDR_IMG = 10'd725;       // Last adress in the array Image representation
    
    localparam WAIT = 6'd51;
    localparam OUT_SIZE = 5'd25;              // Counter Image
    localparam SIZE = 5'd28;                  // Counter Image
    

    ramImg_sim inpu_img(    .clk(clk), 
                            .write_enable(wen_ramImg), 
                            .addr(addr_ramImg), 
                            .dataIn(dataIn_ramImg),    
                            .data1(d_img1), 
                            .data2(d_img2), 
                            .data3(d_img3), 
                            .data4(d_img4), 
                            .data5(d_img5), 
                            .data6(d_img6), 
                            .data7(d_img7), 
                            .data8(d_img8),
                            .data9(d_img9),
                         
                            .check_addr(check_addImgIn),
                            .check_pixel(check_pixel)
                     );
    

    rom_Wconv w(    .addr(buff_addrw),
                    .data(conv_weights)
               );
    
    
    convOneStep_sim conv_slice(     .clk(clk),
                                    .rst(rst_slice), 
                                    .slice(slice), 
                                    .weights(kernel),
                                    .finish_oneStep(finish_slice), 
                                    .y(element),
                                    
                                    .check_rstStep(check_rstStep),
                                    .check_slice(check_sliceIn),
                                    .check_dot(check_dot)
                           );
    
    
    always @(posedge clk, posedge rst)
        if(rst) state <= S0;
        else    state <= next_state;        
    

    always @(posedge clk)
     case(state)
        S0: begin                            // Reset State
            if(load) next_state = S5;
            else    next_state = S1;
      
            wait_counter = 4'd0;
            finish_one = 1'd0;
            finish_conv = 1'b0;
            ready_load = 1'b0;
          
            rst_slice = 1'b1;               // Conv One Step Module  
             
            row = 5'd0;                     // Counter Img Module
            colm = 5'd0;
            buff_colm = 5'd0;
            buff_row = 5'd0;
            addr_img = 10'd0;
            
            addr_w = 4'b0;                  // Rom W. MOdule
            buff_addrw2 = 1'b0;       
            buff_addrw = 1'b0;
                
            wen_ramImg = 1'b0;              // Input Img
            addr_ramImg = 10'd0;
            end
        S1: begin                           // Load Image and Kernel (Dot op.)
            slice = {d_img9, d_img8, d_img7, d_img6, d_img5, d_img4, d_img3, d_img2, d_img1};
            kernel = conv_weights;
            
            finish_conv = 1'b0;
            finish_one = 1'd0;
            
            if( (addr_w == CHANNELS) || !enable )                                    // Finish
             begin
                next_state = S1;
                wait_counter = 4'd0;
             end 
            else next_state = S2;
            end
        S2: begin                           // Enable oneStep module and calculate the Sum
            finish_conv = 1'b0;
            
            rst_slice = 1'b0;
            wait_counter2 = wait_counter;
            
            if( addr_img == LASTADDR_IMG ) addr_w = buff_addrw2 + 1;

            if( colm != (OUT_SIZE) ) next_state = S6;
            else    next_state = S7; 
            end
        S3: begin                           // Wait flag_ok from oneStep module and Load Sum
            addr_img = row * SIZE + colm;
            buff_colm = colm;
            buff_row = row;
            
            if(finish_slice)
            begin
                next_state = S4;         
                conv_out = element;
                finish_one = 1'd1;
            end
            
            finish_conv = (wait_counter == WAIT);
            end
        S4: begin                               // Move forward
            next_state = S1;
            
            rst_slice = 1'b1;
            buff_addrw = addr_w;
            buff_addrw2 = addr_w;
            
            if( addr_img == (LASTADDR_IMG+10'd3) )  begin
                                            colm = 5'd0;
                                            row = 5'd0;
                                            //addr_img = 10'd0;
//                                            flag_kernel = 1'b1;
                                            addr_ramImg = 10'd0;
                                            end
            else addr_ramImg = addr_img;                                                
            wait_counter = wait_counter2 + 4'd1;
          end
        S5: begin
            ready_load = 1'b1;                    
                    
            wen_ramImg = 1'b1;                 // Input Img
            addr_ramImg = addr_loadImg;
            dataIn_ramImg = pixel;
            end
        S6: begin
            next_state = S3;
            colm = buff_colm + 5'b1;
            end
        S7: begin
            next_state = S3;
            
            colm = 5'd0;
            row = buff_row + 5'b1;
            end 
        default: next_state = S0;              //finish_conv = 1'b0;
     endcase
     
    
    // Test signals
    assign check_sliceLoad = slice;
    assign check_wLoad = kernel;
    assign check_sum = element;
    assign check_state = state;
    assign check_addChan = buff_addrw;
    assign check_waitCounter = wait_counter;
    
    assign check_colm = colm;
    assign check_row = row;
    
    assign check_pixel = dataIn_ramImg;
    
endmodule