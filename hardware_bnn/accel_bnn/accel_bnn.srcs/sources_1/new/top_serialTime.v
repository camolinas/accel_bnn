`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07/07/2021 05:02:53 PM
// Design Name: 
// Module Name: top_serialTime
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


    // k17 -> 125 MHz = 8 ns
    // 0.5 s -> 62.5 M  
    // 2**27 = ~134 M
    
    // Want to interface to 115200 baud UART
    // 125000000 / 115200 = 1085 Clocks Per Bit.
    
    // Example
    // Testbench uses a 10 MHz clock
    // Want to interface to 115200 baud UART
    // 10000000 / 115200 = 87 Clocks Per Bit.


module top_serialTime(

    input clk,
    input rst,
    
    input select,
    
    input r_Rx_Serial,
    
    output [6:0]display_out,
    
    // Testing signals
    output check_led,
    output reg check_init
    
    );
    
    reg [2:0]state = 0;
    reg [2:0]next_state = 0;
    
    // Accel BNN
    reg rst_accel;
    reg load;
    reg [7:0]pixel;
    reg [9:0]addr_loadImg;
    reg [3:0]buff_class;
    
    // Img. Addressing 
    reg [9:0]buff_addrImg;
    reg [9:0]buff_addrImg2;
    
    wire [3:0]class;
    wire finish_accel;
    wire ready_load;
    
    // RX UART
    wire [7:0]w_Rx_Byte;
    wire rx_ok;
    
    
    parameter c_CLKS_PER_BIT = 11'd1085;
    
    parameter S0 = 3'd0;
    parameter S1 = 3'd1;
    parameter S2 = 3'd2;
    parameter S3 = 3'd3;
    parameter S4 = 3'd4;
    parameter S5 = 3'd5;
    parameter S6 = 3'd6;
    parameter S7 = 3'd7;
    
    // Auxiliar LEDs
    reg led = 0;
    reg led2 = 0;
    
    
    accel_bnn accel(    .clk( clk ),
                        .rst( rst_accel ),
                        .load( load ),
                        .addr_loadImg( addr_loadImg ),
                        .pixel( pixel ),
                        .ready_load( ready_load ),
                        .finish_accel( finish_accel ),
                        .class( class )
                        
                  );                  
                        
    
    uart_rx #(.CLKS_PER_BIT(c_CLKS_PER_BIT)) UART_RX_INST
        (   .i_Clock( clk ),
            .i_Rx_Serial( r_Rx_Serial ),
            .o_Rx_DV( rx_ok ),
            .o_Rx_Byte( w_Rx_Byte )
         );
    
    
    deco_display display(   .number( buff_class ),
                            .display( display_out )
                        );
     
    
    always @(posedge clk)
        if(rst) state <= S0;
        else    state <= next_state;
    

    always @(posedge clk)
        case(state)
            S0: begin
                if(select) next_state <= S4;
                else    next_state <= S1;
                
                rst_accel <= 1'b1;
                load <= 1'b1;
                
                // Addr Load Img.
                addr_loadImg <= 10'd0;
                buff_addrImg <= 10'd0;


                check_init <= 1'd0;
                led <= 1'b0;
//                led2 <= 1'b0;
                
                buff_class <= 4'd10;
                end
            S1: begin                
                if( rx_ok && ready_load )  next_state <= S2;  
                
                rst_accel <= 1'b0;
                
                addr_loadImg <= buff_addrImg;
                buff_addrImg2 <= buff_addrImg;
                end
            S2: begin                
                if(addr_loadImg == 10'd783) next_state <= S3;
                else    next_state <= S1;

                pixel <= w_Rx_Byte;
                buff_addrImg <= buff_addrImg2 + 10'd1;
                end  
            S3: begin
                next_state <= S4;
                
                rst_accel <= 1'b1;
                load <= 1'b0; 
                end
            S4: begin
                next_state <= S5;
                
                rst_accel <= 1'b1;
                load <= 1'b0;
                
                // Addr Load Img.
                addr_loadImg <= 10'd0;
                buff_addrImg <= 10'd0;
                end
            S5: begin
                if(finish_accel) next_state <= S6;
                else next_state <= S5;
                
                rst_accel <= 1'b0;
                
                check_init <= 1'd1;
                led <= 1'b1;
                end
            S6: begin
                buff_class <= class;
                check_init <= 1'd0;
                end
            default: next_state <= S0;
        endcase        
        

    // Testing LEDs
    assign check_led = led;
    
endmodule