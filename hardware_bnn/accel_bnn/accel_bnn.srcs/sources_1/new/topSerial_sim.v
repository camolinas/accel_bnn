`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06/15/2021 05:22:37 PM
// Design Name: 
// Module Name: topSerial_sim
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

    // k17 -> 125 MHz = 8 ns
    // 0.5 s -> 62.5 M  
    // 2**27 = ~134 M
    
    // Want to interface to 115200 baud UART
    // 125000000 / 115200 = 1085 Clocks Per Bit.
    
    // Example
    // Testbench uses a 10 MHz clock
    // Want to interface to 115200 baud UART
    // 10000000 / 115200 = 87 Clocks Per Bit.

module topSerial_sim(

    input clk,
    input rst,
    
    input select,
    
    input r_Rx_Serial,
    output o_Tx_Serial,
    
    output [6:0]display_out,
    
    // Testing signals
    output check_led,
    output check_clk,
    output check_rst,
    output reg check_init,
    output check_rx_ok,
    output check_tx_Done,
    
    // Debug dignals
    output [2:0]check_state,
//    output [9:0]check_addrLoadRom,
    output [7:0]check_pixelIn,
    output [7:0]check_pixel,
    
    output check_okAccel,
    output [3:0]check_class,
      
    output [3:0]check_stateAccel,
    
    // Test Signals First Stage
    output [5:0]check_addrInmax,
    output [11:0]check_inmax0, 
    output [11:0]check_inmax1, [11:0]check_inmax2, [11:0]check_inmax3, [11:0]check_inmax4, [11:0]check_inmax5, [11:0]check_inmax6, [11:0]check_inmax7,
    
    output [71:0]check_convSlice,
    output [8:0]check_convW,
    output [4:0]check_colm,
    output [4:0]check_row,
    output [9:0]check_convAddr,
    output [2:0]check_convState,
    output [3:0]check_convChan,
    output [11:0]check_conv,
    output  check_okConv, 
    output [5:0]check_convWait,
    
    output [3:0]check_stateStage,
    output [11:0]check_max0, [11:0]check_max1, [11:0]check_max2, [11:0]check_max3, [11:0]check_max4, 
    output [11:0]check_max5,
    output [11:0]check_max6, 
    output [11:0]check_max7, [11:0]check_max8, [11:0]check_max9, [11:0]check_max10, [11:0]check_max11, [11:0]check_max12,
    
////    // Batch Module
    output [2:0]check_stateBatch,
    output [10:0]check_addrBatchW,
    output [12:0]check_elBatch,
    output check_weBatchMem,
    output [12:0]check_resultBatch,
    
////    // Dense
    output [2:0]check_denseState,
    output check_okOneDense,
    output [7:0]check_addrInDense,
    output [9:0]check_denseAddr,
    output [38:0]check_wDense,
    output [38:0]check_andDense, 
    output [8:0]check_sumDense,
    output [8:0]check_class0, 
    output [8:0]check_class1, 
    output [8:0]check_class2,
    output [8:0]check_class3, 
    output [8:0]check_class4, 
    output [8:0]check_class5, 
    output [8:0]check_class6, 
    output [8:0]check_class7,
    output [8:0]check_class8, 
    output [8:0]check_class9,
    output check_okDense
    );
    
    reg [2:0]state = 0;
    reg [2:0]next_state = 0;
    
    
    // Accel BNN
    reg rst_accel;
    reg load;
    reg [7:0]pixel;
    reg [9:0]addr_loadImg;
    reg [3:0]buff_class;
    
    // ROM imgTop
    reg [9:0]buff_addrImg;
    reg [9:0]buff_addrImg2;
    
    wire [3:0]class;
    wire finish_accel;
    wire ready_load;
    
    // RX UART
    wire [7:0]w_Rx_Byte;
    wire rx_ok;
    
    // TX UART
    reg r_Tx_DV = 0;
    reg [7:0]r_Tx_Byte = 0;
    wire w_Tx_Done;
    
    // UART Checking
    wire [11:0]check_inResultConv;
    wire check_oneConv;
    //wire [7:0]check_pixel;
    
    parameter c_CLKS_PER_BIT = 11'd1085;
    
    parameter S0 = 3'd0;
    parameter S1 = 3'd1;
    parameter S2 = 3'd2;
    parameter S3 = 3'd3;
    parameter S4 = 3'd4;
    parameter S5 = 3'd5;
    parameter S6 = 3'd6;
    parameter S7 = 3'd7;
    
    // Auxiliar LEDs
    reg led = 0;
    reg led2 = 0;
    
    
    accelBnn_sim accel(     .clk( clk ),
                            .rst( rst_accel ),
                            .load( load ),
                            .addr_loadImg( addr_loadImg ),
                            .pixel( pixel ),
                            .ready_load( ready_load ),
                            .finish_accel( finish_accel ),
                            .class( class ),
                            
                            .check_inResultConv( check_inResultConv ),
                            .check_pixel( check_pixel ),
                            .check_oneConv( check_oneConv ),
                            .check_state( check_stateAccel ),
                            .check_addrInmax( check_addrInmax ),
                            .check_inmax0( check_inmax0 ),
                            .check_inmax1( check_inmax1 ),
                            .check_inmax2( check_inmax2 ),
                            .check_inmax3( check_inmax3 ),
                            .check_inmax4( check_inmax4 ),
                            .check_inmax5( check_inmax5 ),
                            .check_inmax6( check_inmax6 ),
                            .check_inmax7( check_inmax7 ),
                            .check_convSlice( check_convSlice ),
                            .check_convW( check_convW ),
                            .check_colm( check_colm ),
                            .check_row( check_row ),
                            .check_convAddr( check_convAddr ),
                            .check_convState( check_convState ),
                            .check_convChan( check_convChan ), 
                            .check_conv( check_conv ), 
                            .check_okConv( check_okConv ),
                            .check_convWait( check_convWait ),
                            .check_stateStage( check_stateStage ), 
                            .check_max0( check_max0 ),
                            .check_max1( check_max1 ),
                            .check_max2( check_max2 ),
                            .check_max3( check_max3 ),
                            .check_max4( check_max4 ),
                            .check_max5( check_max5 ),
                            .check_max6( check_max6 ),
                            .check_max7( check_max7 ),
                            .check_max8( check_max8 ),
                            .check_max9( check_max9 ),
                            .check_max10( check_max10 ),
                            .check_max11( check_max11 ),
                            .check_max12( check_max12 ),
                            .check_stateBatch( check_stateBatch ),
                            .check_addrBatchW( check_addrBatchW ),
                            .check_elBatch( check_elBatch ),
                            .check_weBatchMem( check_weBatchMem ),
                            .check_resultBatch( check_resultBatch ),
                            .check_denseState( check_denseState ), 
                            .check_okOneDense( check_okOneDense ), 
                            .check_addrInDense( check_addrInDense ), 
                            .check_denseAddr( check_denseAddr ), 
                            .check_wDense( check_wDense ),
                            .check_andDense( check_andDense ),
                            .check_sumDense( check_sumDense ),
                            .check_class0( check_class0 ),
                            .check_class1( check_class1 ),
                            .check_class2( check_class2 ),
                            .check_class3( check_class3 ),
                            .check_class4( check_class4 ),
                            .check_class5( check_class5 ),
                            .check_class6( check_class6 ),
                            .check_class7( check_class7 ),
                            .check_class8( check_class8 ),
                            .check_class9( check_class9 ),
                            .check_okDense( check_okDense )
                  );                  
                        

    uart_rx #(.CLKS_PER_BIT(c_CLKS_PER_BIT)) UART_RX_INST
        (   .i_Clock( clk ),
            .i_Rx_Serial( r_Rx_Serial ),
            .o_Rx_DV( rx_ok ),
            .o_Rx_Byte( w_Rx_Byte )
         );
    
    
    deco_display display(   .number( buff_class ),
                            .display( display_out )
                        );
    
                        
    uart_tx #(.CLKS_PER_BIT(c_CLKS_PER_BIT)) UART_TX_INST
    (.i_Clock( clk ),
     .i_Tx_DV( r_Tx_DV ),
     .i_Tx_Byte( r_Tx_Byte ),
     .o_Tx_Active(),
     .o_Tx_Serial( o_Tx_Serial ),
     .o_Tx_Done( w_Tx_Done )
     );
    
    
    always @(posedge clk)
        if(rst) state <= S0;
        else    state <= next_state;
    

    always @(posedge clk)
        case(state)
            S0: begin
                if(select) next_state <= S4;
                else    next_state <= S1;
                
                rst_accel <= 1'b1;
                load <= 1'b1;
                
                // Addr Load Img.
                addr_loadImg <= 10'd0;
                buff_addrImg <= 10'd0;

                check_init <= 1'd0;
//                led <= 1'b0;
//                led2 <= 1'b0;
                
                r_Tx_DV <= 1'b0;
                r_Tx_Byte <= 8'b0;
                
                buff_class <= 4'd10;
                end
            S1: begin                
                if( rx_ok && ready_load )  next_state <= S2;
//                else    next_state <= S1;  
                
                rst_accel <= 1'b0;

                addr_loadImg <= buff_addrImg;
                buff_addrImg2 <= buff_addrImg;
                
                r_Tx_DV <= 1'b0;
                end
            S2: begin                
                if(addr_loadImg == 10'd783) next_state <= S3;
                else    next_state <= S7;

                pixel <= w_Rx_Byte;
                buff_addrImg <= buff_addrImg2 + 10'd1;
                end  
            S7: begin
                if(w_Tx_Done) begin
                              next_state <= S1;
                              r_Tx_DV <= 1'b0;
                              end
                else begin
                     next_state <= S7;
                     r_Tx_DV <= 1'b1;
                     end

                r_Tx_Byte <= check_pixel;
                end  
            S3: begin
                next_state <= S4;
                
                rst_accel <= 1'b1;
                load <= 1'b0; 
                end
            S4: begin
                next_state <= S5;
                
                rst_accel <= 1'b1;
                load <= 1'b0;
                
                // Addr Load Img.
                addr_loadImg <= 10'd0;
                buff_addrImg <= 10'd0;
                end
            S5: begin
                if(finish_accel) next_state <= S6;
                else next_state <= S5;
                
                rst_accel <= 1'b0;
                
                check_init <= 1'd1;
//                if(check_oneConv) led <= 1'b1;
//                if(check_okConv) led2 <= 1'b1;
//                if(select) r_Tx_Byte = check_inResultConv[7:0];
                end
            S6: begin
                buff_class <= class;
                end
            default: next_state <= S0;
        endcase
        
        
    assign check_state = state;
    assign check_pixelIn = pixel;
    assign check_class = class;
    assign check_okAccel = finish_accel;

    // Testing LEDs
    assign check_clk = led2;
    assign check_rst = finish_accel;
    assign check_led = led;
    
    assign check_rx_ok = rx_ok;
    assign check_tx_Done = w_Tx_Done;
    
endmodule