`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06/15/2021 08:30:10 PM
// Design Name: 
// Module Name: rom_Wconv
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module rom_Wconv (
    input [3:0]addr,
    
    output [8:0] data
    );
    
    reg [8:0] mem [11:0]; 
    
    initial
     begin
        $readmemb("/home/camilo/Documents/accelerator_BNN/code_test_acc/conv_bnn_weights2.mem", mem);
     end
     
    assign data = mem[addr];
    
endmodule