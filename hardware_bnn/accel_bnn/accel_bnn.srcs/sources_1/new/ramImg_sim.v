`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06/15/2021 08:27:17 PM
// Design Name: 
// Module Name: ramImg_sim
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ramImg_sim #(parameter width = 4'd8, SIZE = 5'd28)(
    input clk,
    input write_enable,
    input [9:0]addr,
    input [width-1:0]dataIn,
    
    output [width-1:0] data1,
    output [width-1:0] data2,
    output [width-1:0] data3,
    output [width-1:0] data4,
    output [width-1:0] data5,
    output [width-1:0] data6,
    output [width-1:0] data7,
    output [width-1:0] data8,
    output [width-1:0] data9,
    
    // Test signals
    output [9:0] check_addr,
    output [7:0] check_pixel
    );
    
    reg [width-1:0] mem [783:0];
    
//    initial
//     begin
//        //$readmemb("/home/camilo/Documents/accelerator_BNN/code_test_acc/input28_test.mem", mem);
//        $readmemb("/home/camilo/Documents/accelerator_BNN/code_test_acc/img28x28_n2_test.mem", mem);
//     end
     
    always @(posedge clk)
        if(write_enable) mem[addr] <= dataIn;

  
    assign data1 = mem[addr];       // First row
    assign data2 = mem[addr+1];
    assign data3 = mem[addr+2];
    assign data4 = mem[addr+SIZE];     // Second row
    assign data5 = mem[addr+SIZE+1];     // 4+1
    assign data6 = mem[addr+SIZE+2];     // 4+2
    assign data7 = mem[addr+2*SIZE];     // Third row
    assign data8 = mem[addr+2*SIZE+1];     // 8+1
    assign data9 = mem[addr+2*SIZE+2];    // 8+2

    assign check_addr = addr;
    assign check_pixel = dataIn;
    
endmodule     