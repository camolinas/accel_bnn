`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06/15/2021 08:48:58 PM
// Design Name: 
// Module Name: sign_ineq
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module sign_ineq (
    input clk,
    input rst,
    input [11:0]element,
    input [11:0]alpha,
    
    output reg activation

    );
    
    reg [1:0]state;
    reg [1:0]next_state;
    
    reg [11:0]buff_element;     // Garantee the hold the right input in every step
    
    localparam S0 = 2'd0;        // Compare with CTE reference  
    localparam S1 = 2'd1;        // Compare input/weigth
    localparam S2 = 2'd2;        // Burn one cycle to keep the symmetry
    
    always @(posedge clk)
        if(rst) state <= S0;
        else    state <= next_state;


    always @(posedge clk)
        case(state)
            S0: begin
                if( alpha == 12'd2048 ) next_state = S2;
                else    next_state = S1;
                
                buff_element = element;
                end
            S1: begin
                if( buff_element > alpha ) activation = 1'b1;
                else activation = 1'b0;

                end
            S2: begin
            
                activation = 1'b1;
                end
            default: next_state = S0;
        endcase

    
endmodule