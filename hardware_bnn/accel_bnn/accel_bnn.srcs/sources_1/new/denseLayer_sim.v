`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06/15/2021 06:32:12 PM
// Design Name: 
// Module Name: denseLayer_sim
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module denseLayer_sim(
    input clk,
    input rst,
    input enable,
    input [38:0]in_array,
    
    output reg finish_dense,
    output reg finish_one,
    
//    output reg [10:0]out
    
    output reg [8:0]result0, 
    output reg [8:0]result1, 
    output reg [8:0]result2, 
    output reg [8:0]result3, 
    output reg [8:0]result4, 
    output reg [8:0]result5, 
    output reg [8:0]result6, 
    output reg [8:0]result7,
    output reg [8:0]result8, 
    output reg [8:0]result9,
    
    output [2:0]check_state,
    output [9:0]check_addr,
    output [38:0]check_w0,
    output [38:0]check_and0,
    output [8:0]check_sum0
    );
    
    //Just for test IMPLEMENTATION
    //reg [8:0] result0, result1, result2, result3, result4, result5, result6, result7, result8, result9;
    
    reg [2:0]state;
    reg [2:0]next_state;
    
    reg [38:0] and0, and1, and2, and3, and4, and5, and6, and7, and8, and9;
    reg [8:0] sum0, sum1, sum2, sum3, sum4, sum5, sum6, sum7, sum8, sum9;
    reg [8:0] buff_result0, buff_result1, buff_result2, buff_result3, buff_result4, buff_result5, buff_result6, buff_result7, buff_result8, buff_result9;    
    
    // ROM Dense Weights
    reg [9:0]addr_weights;
    reg [9:0]buff_addrW;
    reg [9:0]buff_addrW2;
    
    // ROM Dense Weights
    wire [38:0] w0, w1, w2, w3, w4, w5, w6, w7, w8, w9;
    
    parameter S0 = 3'd0;        // Reset
    parameter S1 = 3'd1;        // 
    parameter S2 = 3'd2;        // 
    parameter S3 = 3'd3;        //
    parameter S4 = 3'd4;        //
    parameter S5 = 3'd5;        // 
    
    rom_wdense denseW(addr_weights, w0, w1, w2, w3, w4, w5, w6, w7, w8, w9);
    
    always @(posedge clk)
        if(rst) state <= S0;
        else    state <= next_state;
    

    always @(posedge clk)
        case(state)
            S0: begin
                next_state = S1;
                finish_one = 1'b0;
                finish_dense = 1'b0;
                
                buff_result0 = 9'b0;
                buff_result1 = 9'b0;
                buff_result2 = 9'b0;
                buff_result3 = 9'b0;
                buff_result4 = 9'b0;
                buff_result5 = 9'b0;
                buff_result6 = 9'b0;
                buff_result7 = 9'b0;
                buff_result8 = 9'b0;
                buff_result9 = 9'b0;
                
                addr_weights = 10'd0;
                buff_addrW = 10'd0;
                buff_addrW2 = 10'd0;
                end
            S1: begin
                if(enable) next_state = S2;
                else    next_state = S1;
                
                and0 = w0 & in_array;
                and1 = w1 & in_array;
                and2 = w2 & in_array;
                and3 = w3 & in_array;
                and4 = w4 & in_array;
                and5 = w5 & in_array;
                and6 = w6 & in_array;
                and7 = w7 & in_array;
                and8 = w8 & in_array;
                and9 = w9 & in_array; 
                
                finish_one = 1'b0;
                end
            S2: begin
                next_state = S3;               

                sum0 = and0[0] + and0[1] + and0[2] + and0[3] + and0[4] + and0[5] + and0[6] + and0[7] + and0[8] + and0[9] + and0[10] + and0[11] + and0[12] + and0[13] + and0[14] + and0[15] + and0[16] + and0[17] + and0[18] + and0[19] + and0[20] + and0[21] + and0[22] + and0[23] + and0[24] + and0[25] + and0[26] + and0[27] + and0[28] + and0[29] + and0[30] + and0[31] + and0[32] + and0[33] + and0[34] + and0[35] + and0[36] + and0[37] + and0[38]; // + and0[39]; // + and0[40] + and0[41] + and0[42] + and0[43] + and0[44] + and0[45] + and0[46] + and0[47] + and0[48] + and0[49] + and0[50] + and0[51] + and0[52];  
                sum1 = and1[0] + and1[1] + and1[2] + and1[3] + and1[4] + and1[5] + and1[6] + and1[7] + and1[8] + and1[9] + and1[10] + and1[11] + and1[12] + and1[13] + and1[14] + and1[15] + and1[16] + and1[17] + and1[18] + and1[19] + and1[20] + and1[21] + and1[22] + and1[23] + and1[24] + and1[25] + and1[26] + and1[27] + and1[28] + and1[29] + and1[30] + and1[31] + and1[32] + and1[33] + and1[34] + and1[35] + and1[36] + and1[37] + and1[38]; // + and1[39]; // + and1[40] + and1[41] + and1[42] + and1[43] + and1[44] + and1[45] + and1[46] + and1[47] + and1[48] + and1[49] + and1[50] + and1[51] + and1[52];
                sum2 = and2[0] + and2[1] + and2[2] + and2[3] + and2[4] + and2[5] + and2[6] + and2[7] + and2[8] + and2[9] + and2[10] + and2[11] + and2[12] + and2[13] + and2[14] + and2[15] + and2[16] + and2[17] + and2[18] + and2[19] + and2[20] + and2[21] + and2[22] + and2[23] + and2[24] + and2[25] + and2[26] + and2[27] + and2[28] + and2[29] + and2[30] + and2[31] + and2[32] + and2[33] + and2[34] + and2[35] + and2[36] + and2[37] + and2[38]; // + and2[39]; // + and2[40] + and2[41] + and2[42] + and2[43] + and2[44] + and2[45] + and2[46] + and2[47] + and2[48] + and2[49] + and2[50] + and2[51] + and2[52];
                sum3 = and3[0] + and3[1] + and3[2] + and3[3] + and3[4] + and3[5] + and3[6] + and3[7] + and3[8] + and3[9] + and3[10] + and3[11] + and3[12] + and3[13] + and3[14] + and3[15] + and3[16] + and3[17] + and3[18] + and3[19] + and3[20] + and3[21] + and3[22] + and3[23] + and3[24] + and3[25] + and3[26] + and3[27] + and3[28] + and3[29] + and3[30] + and3[31] + and3[32] + and3[33] + and3[34] + and3[35] + and3[36] + and3[37] + and3[38]; // + and3[39]; // + and3[40] + and3[41] + and3[42] + and3[43] + and3[44] + and3[45] + and3[46] + and3[47] + and3[48] + and3[49] + and3[50] + and3[51] + and3[52];
                sum4 = and4[0] + and4[1] + and4[2] + and4[3] + and4[4] + and4[5] + and4[6] + and4[7] + and4[8] + and4[9] + and4[10] + and4[11] + and4[12] + and4[13] + and4[14] + and4[15] + and4[16] + and4[17] + and4[18] + and4[19] + and4[20] + and4[21] + and4[22] + and4[23] + and4[24] + and4[25] + and4[26] + and4[27] + and4[28] + and4[29] + and4[30] + and4[31] + and4[32] + and4[33] + and4[34] + and4[35] + and4[36] + and4[37] + and4[38]; // + and4[39]; // + and4[40] + and4[41] + and4[42] + and4[43] + and4[44] + and4[45] + and4[46] + and4[47] + and4[48] + and4[49] + and4[50] + and4[51] + and4[52];
                sum5 = and5[0] + and5[1] + and5[2] + and5[3] + and5[4] + and5[5] + and5[6] + and5[7] + and5[8] + and5[9] + and5[10] + and5[11] + and5[12] + and5[13] + and5[14] + and5[15] + and5[16] + and5[17] + and5[18] + and5[19] + and5[20] + and5[21] + and5[22] + and5[23] + and5[24] + and5[25] + and5[26] + and5[27] + and5[28] + and5[29] + and5[30] + and5[31] + and5[32] + and5[33] + and5[34] + and5[35] + and5[36] + and5[37] + and5[38]; // + and5[39]; // + and5[40] + and5[41] + and5[42] + and5[43] + and5[44] + and5[45] + and5[46] + and5[47] + and5[48] + and5[49] + and5[50] + and5[51] + and5[52];
                sum6 = and6[0] + and6[1] + and6[2] + and6[3] + and6[4] + and6[5] + and6[6] + and6[7] + and6[8] + and6[9] + and6[10] + and6[11] + and6[12] + and6[13] + and6[14] + and6[15] + and6[16] + and6[17] + and6[18] + and6[19] + and6[20] + and6[21] + and6[22] + and6[23] + and6[24] + and6[25] + and6[26] + and6[27] + and6[28] + and6[29] + and6[30] + and6[31] + and6[32] + and6[33] + and6[34] + and6[35] + and6[36] + and6[37] + and6[38]; // + and6[39]; // + and6[40] + and6[41] + and6[42] + and6[43] + and6[44] + and6[45] + and6[46] + and6[47] + and6[48] + and6[49] + and6[50] + and6[51] + and6[52];
                sum7 = and7[0] + and7[1] + and7[2] + and7[3] + and7[4] + and7[5] + and7[6] + and7[7] + and7[8] + and7[9] + and7[10] + and7[11] + and7[12] + and7[13] + and7[14] + and7[15] + and7[16] + and7[17] + and7[18] + and7[19] + and7[20] + and7[21] + and7[22] + and7[23] + and7[24] + and7[25] + and7[26] + and7[27] + and7[28] + and7[29] + and7[30] + and7[31] + and7[32] + and7[33] + and7[34] + and7[35] + and7[36] + and7[37] + and7[38]; // + and7[39]; // + and7[40] + and7[41] + and7[42] + and7[43] + and7[44] + and7[45] + and7[46] + and7[47] + and7[48] + and7[49] + and7[50] + and7[51] + and7[52];
                sum8 = and8[0] + and8[1] + and8[2] + and8[3] + and8[4] + and8[5] + and8[6] + and8[7] + and8[8] + and8[9] + and8[10] + and8[11] + and8[12] + and8[13] + and8[14] + and8[15] + and8[16] + and8[17] + and8[18] + and8[19] + and8[20] + and8[21] + and8[22] + and8[23] + and8[24] + and8[25] + and8[26] + and8[27] + and8[28] + and8[29] + and8[30] + and8[31] + and8[32] + and8[33] + and8[34] + and8[35] + and8[36] + and8[37] + and8[38]; // + and8[39]; // + and8[40] + and8[41] + and8[42] + and8[43] + and8[44] + and8[45] + and8[46] + and8[47] + and8[48] + and8[49] + and8[50] + and8[51] + and8[52];
                sum9 = and9[0] + and9[1] + and9[2] + and9[3] + and9[4] + and9[5] + and9[6] + and9[7] + and9[8] + and9[9] + and9[10] + and9[11] + and9[12] + and9[13] + and9[14] + and9[15] + and9[16] + and9[17] + and9[18] + and9[19] + and9[20] + and9[21] + and9[22] + and9[23] + and9[24] + and9[25] + and9[26] + and9[27] + and9[28] + and9[29] + and9[30] + and9[31] + and9[32] + and9[33] + and9[34] + and9[35] + and9[36] + and9[37] + and9[38]; // + and9[39]; // + and9[40] + and9[41] + and9[42] + and9[43] + and9[44] + and9[45] + and9[46] + and9[47] + and9[48] + and9[49] + and9[50] + and9[51] + and9[52];   
                end
            S3: begin
                next_state = S4;
                
                result0 = sum0 + buff_result0;
                result1 = sum1 + buff_result1;
                result2 = sum2 + buff_result2;
                result3 = sum3 + buff_result3;
                result4 = sum4 + buff_result4;
                result5 = sum5 + buff_result5;
                result6 = sum6 + buff_result6;
                result7 = sum7 + buff_result7;
                result8 = sum8 + buff_result8;
                result9 = sum9 + buff_result9;                
                
                buff_addrW = buff_addrW2 + 9'd10;
                end
            S4: begin
                if( buff_addrW == 10'd520 )  next_state = S5;
                else    next_state = S1;
                
                buff_result0 = result0;
                buff_result1 = result1;
                buff_result2 = result2;
                buff_result3 = result3;
                buff_result4 = result4;
                buff_result5 = result5;
                buff_result6 = result6;
                buff_result7 = result7;
                buff_result8 = result8;
                buff_result9 = result9;
                
                addr_weights = buff_addrW;
                buff_addrW2 = buff_addrW;
                finish_one = 1'b1;
                end
            S5: begin
                //out = result0 + result1 + result2 + result3 + result4 + result5 + result6 + result7 + result8 + result9;
                
                finish_dense = 1'b1;
                end
            default next_state = S0;
        endcase
        
        
    assign check_state = state;
    assign check_addr = addr_weights;
    assign check_w0 = w0;
    assign check_and0 = and0;
    assign check_sum0 = sum0;
    
endmodule
