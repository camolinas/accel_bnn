`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06/15/2021 08:31:41 PM
// Design Name: 
// Module Name: conv_oneStep
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module conv_oneStep #(parameter width = 72, S0 = 1'b0, S1 = 1'b1)(
    input clk,
    input rst,
    input [width-1:0] slice,
    input [8:0] weights,

    output reg finish_oneStep,
    output [11:0] y
    
    );
    
    reg state;
    reg next_state;
    
    reg [width-1:0]buff_dot;        // Array after multiplation (Img_slice*Weights)
    
    wire [width-1:0]dot_out;        
    
    conv_dot dot(slice, weights, dot_out);
    conv_sum sum(buff_dot, y);
    
    
    always @(posedge clk)
        if(rst) state <= S0;    // Dot             
        else    state <= next_state;
         

    always @(posedge clk)
        case(state)
            S0:     // SUM
            begin
                next_state = S1; 
                buff_dot = dot_out;
                finish_oneStep = 1'b0; 
            end
            S1:     // Complete
            begin           
                finish_oneStep = 1'b1;
            end
        endcase   

    
endmodule