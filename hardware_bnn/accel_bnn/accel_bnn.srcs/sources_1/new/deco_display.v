`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06/15/2021 05:59:20 PM
// Design Name: 
// Module Name: deco_display
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

//      a
//     ___
//  f |   | b
//      g
//     ___
//  e |   | c
//     ___
//      d

module deco_display(
    input [3:0]number,
    output reg [6:0]display );
      
    parameter N0 = 4'd0;
    parameter N1 = 4'd1;
    parameter N2 = 4'd2;
    parameter N3 = 4'd3;
    parameter N4 = 4'd4;
    parameter N5 = 4'd5;
    parameter N6 = 4'd6;
    parameter N7 = 4'd7;
    parameter N8 = 4'd8;
    parameter N9 = 4'd9;
    
    always @(*)
        case(number)     //  abcdefg
            N0: display = 7'b0000001;       //7'b1111110;
            N1: display = 7'b1001111;
            N2: display = 7'b0010010;
            N3: display = 7'b0000110;       //7'b1111001;
            N4: display = 7'b1001100;       //7'b0110011;
            N5: display = 7'b0100100;       //7'b1011011;
            N6: display = 7'b0100000;       //7'b1011111;
            N7: display = 7'b0001111;       //7'b1110000;
            N8: display = 7'b0000000;       //7'b1111111;
            N9: display = 7'b0001100;       //7'b1110011;
            default:  display = 7'b1111110; //7'b0000001;
        endcase
    
endmodule
