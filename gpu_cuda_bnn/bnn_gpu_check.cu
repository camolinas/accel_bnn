#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>
#include <cuda.h>
#include <cuda_runtime.h>
#include <chrono>

#include <iostream>
#include <fstream>
#include <string>

using namespace std;


void dense_cpu(int *batch, int *w);

int get_prediction(int *predicted);

__global__ void conv(int *img, int *kernel, int *out) {
	int row = blockIdx.x * blockDim.x + threadIdx.x;
	int colm = blockIdx.y * blockDim.y + threadIdx.y;	
	
	int index = 0;	
	int index2 = 0;
	int offset_channel = 0;
	int offset_out = 0;
	
	int auxRow = 0;
	int auxColm = 0;

	int dot[9];
	for( int i = 0; i < 9; i++)	dot[9] = 0; 

	if( row < 26 && colm < 26 ){
		index = row * 28 + colm;
		index2 = row * 26 + colm;
		
		for( int c = 0; c < 12; c++ ){
			for( int i = 0; i < 9; i++ ){	
				dot[i] = img[index] * kernel[ i+offset_channel ];

				if( auxColm == 2 ){
					auxColm = 0;
					if( auxRow == 2 ){
						auxRow = 0;
					}else{
						auxRow = auxRow + 1;
					}
				}else{
					auxColm = auxColm + 1;
				}
			
				index = (row+auxRow) * 28 + colm+auxColm;	
			}
	
			for( int i = 1; i < 9; i++ )	dot[0] = dot[0] + dot[i]; 
			out[ index2+offset_out ] = dot[0]; 

			offset_channel = offset_channel + 9;
			offset_out = offset_out + (26*26);
		}
	}		
}


__global__ void maxpool(int *conv_out, int *out) {
	int row = blockIdx.x * blockDim.x + threadIdx.x;
	int colm = blockIdx.y * blockDim.y + threadIdx.y;	
	
	int index = 0;	
	int index2 = 0;
	int offset_out = 0;
	int offset_channels = 0;
	
	int auxRow = 0;
	int auxColm = 0;

	int max[4];
	for( int i = 0; i < 4; i++)	max[i] = 0; 

	if( row < 13 && colm < 13 ){
		//index = (2*row) * 26 + (2*colm);
		index2 = row * 13 + colm;
		
		for( int c = 0; c < 12; c++ ){
			for( int i = 0; i < 4; i++ ){	
				
				if( i == 1 || i == 3 ){
					auxColm = 1;
				}else{
					auxColm = 0;
			  }	
				if( i == 2 || i == 3 ){
					auxRow = 1;
				}else{
					auxRow = 0;
			  }	
			
				index = (2*row+auxRow) * 26 + (2*colm+auxColm) + offset_channels;	
				
				//printf("idx %u: %u, %u (%u, %u) \n", i, index, index2, row, colm);
				max[i] = conv_out[index];
			}
	
			for( int i = 1; i < 4; i++ ){
				if( max[0] < max[i] ){
					max[0] = max[i];
				}
			}

			out[ index2+offset_out ] = max[0]; 
			
			offset_out = offset_out + (13*13);
			offset_channels = offset_channels + (26*26);
		}
	}	
}


__global__ void sign_ineq(int *maxpool_out, int *alpha, int *out){
	int idx = blockIdx.x * blockDim.x + threadIdx.x;
	
	if( idx < 2028 ){
		if( alpha[idx] == 2048 ){
			out[idx] = 1;

		}else{

			if( maxpool_out[idx] > alpha[idx] ){
				out[idx] = 1;
			}else{
				out[idx] = 0;				
			}
		}

	}
}


__global__ void dense(int *batch_out, int *w, int *out){
	int colm = blockIdx.x * blockDim.x + threadIdx.x;

	if( colm < 2028 ){
		for( int i = 0; i < 10; i++){
			out[ 2028*i + colm ] = w[ 2028*i + colm ] * batch_out[ colm ];
		}
	}
} 

__global__ void sum(int *out_dense, int *prediction){
	int index = threadIdx.x;

  __shared__ int reduction[ 1024 ];
  int regi = 0;
	int offset = 0;
	
	for( int r = 0; r < 10; r++ ){
		if( index < 1024 ){
	  	reduction[ index ] = 0; 
			regi = out_dense[ index+offset ];
		}
	
		if( index < 1004 ){
			regi = regi + out_dense[ index+1024+offset ];
		}
	
  	reduction[ index ] = regi;
  	__syncthreads();  
    
  	int dim = blockDim.x/2;
  	while( dim != 0 ){
    	if( index < dim ){
      	reduction[ index ] = reduction[ index ] + reduction[ index+dim ]; 
    	}   
  	__syncthreads();
  	dim /= 2;
  	}   

  	if( index == 0 ){  
    	prediction[r] = reduction[ 0 ];  
  	}
		offset = offset + 2028;
	}
}

int main(){

	// Conv. Variables
	int *img, *kernel;
	int *d_img, *d_kernel;

	// Max. Pooling Variables
	int *d_convOut, *d_maxpoolOut;

	// BatchNorm. Variables
	int *alpha;
	int *d_alpha, *d_batchOut;

	// Dense Variables
	int *w_dense, *prediction;
	int *d_wDense, *d_prediction;
	int *d_outDense_large;

	// General Variables
	int classPredicted = 0;
	fstream file;
	int dataInt = 0;

	// Testing Variables (Uncomment to Check Output Layers)
	int *conv_out;
	int *maxpool_out;
	int *batch_out;

	// Allocate host memory
	img    = (int*)malloc(sizeof(int) * 28*28);
	kernel = (int*)malloc(sizeof(int) * 9*12);

	conv_out = (int*)malloc(sizeof(int) * 26*26*12);
	maxpool_out = (int*)malloc(sizeof(int) *13*13*12);

	alpha = (int*)malloc(sizeof(int) *13*13*12);
	batch_out = (int*)malloc(sizeof(int) *13*13*12);

	w_dense = (int*)malloc(sizeof(int) *13*13*12*10);
	prediction = (int*)malloc(sizeof(int) *10);
 
	
	// Initialize host arrays
	string PATH_IMG = "/home/gfnun/Documents/camilo/sharedata/images_dec/img28x28_n2.txt";
	string PATH_CONVW = "/home/gfnun/Documents/camilo/sharedata/weights/conv_w.txt";
	string input;
	
	file.open(PATH_IMG, ios::in);
	for(int i = 0; i < (28*28); i++){
		getline(file, input);
		dataInt = stoi(input);
	
		img[i] = dataInt;
	}
	file.close();

	file.open(PATH_CONVW, ios::in);
	for(int i = 0; i < (9*12); i++){
		getline(file, input);
		dataInt = stoi(input);

		kernel[i] = dataInt;
		//cout << "Kernel : " << dataInt << "in: " << input << "\n";
	}
	file.close();

	string PATH_ALPHA = "/home/gfnun/Documents/camilo/sharedata/weights/batch_param_dec.txt";
	
	file.open(PATH_ALPHA, ios::in);
	for(int i = 0; i < (13*13*12); i++){
		getline(file, input);
		dataInt = stoi(input);
	
		alpha[i] = dataInt;
		//cout << "alpha : " << dataInt << "In: " <<  input << "\n";
	}
	file.close();

	string PATH_DENSEW = "/home/gfnun/Documents/camilo/sharedata/weights/dense_param_dec.txt";
	
	file.open(PATH_DENSEW, ios::in);
	for(int i = 0; i < (13*13*12*10); i++){
		getline(file, input);
		dataInt = stoi(input);
	
		w_dense[i] = dataInt;
		//cout << "W Dense : " << dataInt << "In: " <<  input << "\n";
	}
	file.close();


	// Allocate divice memory
	cudaMalloc((void**)&d_img, sizeof(int) * 28*28);
	cudaMalloc((void**)&d_kernel, sizeof(int) * 9*12);

	cudaMalloc((void**)&d_convOut, sizeof(int) * 26*26*12);
	cudaMalloc((void**)&d_maxpoolOut, sizeof(int) *13*13*12);

	cudaMalloc((void**)&d_alpha, sizeof(int) * 13*13*12);
	cudaMalloc((void**)&d_batchOut, sizeof(int) *13*13*12);

	cudaMalloc((void**)&d_wDense, sizeof(int) *13*13*12*10);
	cudaMalloc((void**)&d_prediction, sizeof(int) *10);
	cudaMalloc((void**)&d_outDense_large, sizeof(int) *13*13*12*10);


	// Transfer data H->D memory
	cudaMemcpy(d_img, img, sizeof(int) * 28*28, cudaMemcpyHostToDevice);
	cudaMemcpy(d_kernel, kernel, sizeof(int) * 9*12, cudaMemcpyHostToDevice);

	//cudaMemset(d_maxpoolOut, 0, sizeof(int) );
	cudaMemcpy(d_alpha, alpha, sizeof(int) * 13*13*12, cudaMemcpyHostToDevice);

	cudaMemcpy(d_wDense, w_dense, sizeof(int) * 13*13*12*10, cudaMemcpyHostToDevice);

	// Execute the kernel
	dim3 dimGrid(2, 2);
	dim3 dimBlock(14, 14);
	conv<<<dimGrid, dimBlock>>>(d_img, d_kernel, d_convOut);

	dim3 dimGrid1(2, 2);
	dim3 dimBlock1(7, 7);
	maxpool<<<dimGrid1, dimBlock1>>>(d_convOut, d_maxpoolOut);
	
	sign_ineq<<< 2, 1024 >>>(d_maxpoolOut, d_alpha, d_batchOut);

	dense<<< 2, 1024 >>>(d_batchOut, d_wDense, d_outDense_large);
	sum<<< 1, 1024 >>>(d_outDense_large, d_prediction);


	// Transfer data back D->H memory
	cudaMemcpy(conv_out, d_convOut, sizeof(int)*26*26*12, cudaMemcpyDeviceToHost);
	cudaMemcpy(maxpool_out, d_maxpoolOut, sizeof(int)*13*13*12, cudaMemcpyDeviceToHost);	
	cudaMemcpy(batch_out, d_batchOut, sizeof(int)*13*13*12, cudaMemcpyDeviceToHost);	
	cudaMemcpy(prediction, d_prediction, sizeof(int)*10, cudaMemcpyDeviceToHost);	

	// Visualize Result
	classPredicted = get_prediction(prediction);
	printf("Class predicted = %u \n", classPredicted);


	// Verification (Uncomment to Compare Output Layers)

	printf("---------- CPU Dense ----------\n");
	dense_cpu(batch_out, w_dense);
	printf("---------- GPU Predicton -------\n");

	for( int i = 0; i < (10); i++ ){
		printf("Class [%u] : %u \n", i, prediction[i]);
	}


	string PATH_CONVOUT = "/home/gfnun/Documents/camilo/sharedata/outLayers_dec/conv_out_dec.txt";
	int dif = 0;

	file.open(PATH_CONVOUT, ios::in);
	for(int i = 0; i < (26*26*12); i++){
		getline(file, input);
		dataInt = stoi(input);

		dif = dataInt - conv_out[i];
		if( dif != 0 ){
			cout << "Index: " << i << "  Conv. Out: " << conv_out[i] << "  data: " << dataInt << "\n";
		}
	}
	file.close();
	
	string PATH_MAXOUT = "/home/gfnun/Documents/camilo/sharedata/outLayers_dec/maxpool_out_dec.txt";

	file.open(PATH_MAXOUT, ios::in);
	for(int i = 0; i < (13*13*12); i++){
		getline(file, input);
		dataInt = stoi(input);

		dif = dataInt - maxpool_out[i];
		if( dif != 0 ){
			cout << "Index: " << i << "  Max.Pooling Out: " << maxpool_out[i] << "  data: " << dataInt << "\n";
		}
	}
	file.close();


	// Free Memory
	cudaFree(d_img);
	cudaFree(d_kernel);
	cudaFree(d_convOut);
	cudaFree(d_maxpoolOut);
	cudaFree(d_alpha);
	cudaFree(d_batchOut);
	cudaFree(d_wDense);
	cudaFree(d_outDense_large);
	cudaFree(d_prediction);

	free(img);
	free(kernel);
	free(conv_out);
	free(maxpool_out);
	free(alpha);
	free(batch_out);
	free(w_dense);
	free(prediction);
}


int get_prediction(int *predicted){
	int index_class = 0;
	int save = 0;
	save = predicted[0];	

	for(int i = 0; i < 10; i++){
		if( predicted[i] > predicted[0]){
			predicted[0] = predicted[i];  
			index_class = i;		
		}
	}
	
	predicted[0] = save;
	return index_class;
}

void dense_cpu(int *batch, int *w){

	int save_sum[2028*10];
	int classPredict[10];
	int offset = 0;

	for(int c = 0; c < 10; c++) classPredict[c] = 0;

	for(int c = 0; c < 10; c++){
		for(int i = 0; i < 2028; i++){
			save_sum[i+offset] = batch[i] * w[i+offset];	
		}
		offset = offset + 2028;
	}

	offset = 0;
	for(int c = 0; c < 10; c++){
		for(int i = 0; i < 2028; i++){
			 classPredict[c] = classPredict[c] + save_sum[i+offset];
		}
		offset = offset + 2028;
	}


	for(int c = 0; c < 10; c++){
		printf("Prediction %u = %u \n", c, classPredict[c]);
	}
}
